defmodule BrickcroftWeb.Router do
  use BrickcroftWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :frontend do


  end

  pipeline :admin do
    plug BrickcroftWeb.Plugs.AdminLayout
    plug BrickcroftWeb.Plugs.LoadUser

  end

  scope "/", BrickcroftWeb do
    pipe_through [:browser, BrickcroftWeb.Plugs.LoadUser]

    get "/", PageController, :index

    get "/admin", Admin.SessionController, :new
    post "/admin", Admin.SessionController, :create

    get "/home", PageController, :dashboard

    get "/forgotpassword", Admin.SessionController, :recover_password
    get "/:slug", PageController, :show_by_slug
    post "/forgotpassword", Admin.SessionController, :recover_password_submit
    post "/enquiry", PageController, :contact

    get "/resetpassword/:code", Admin.SessionController, :reset_password
    post "/resetpassword/:code", Admin.SessionController, :reset_password_submit



  end

  scope "/admin", BrickcroftWeb.Admin, as: :admin  do
    pipe_through [:browser, :admin, BrickcroftWeb.Plugs.AuthenticateUser]
    get "/logout", SessionController, :delete
    get "/changepassword", SessionController, :change_password
    post "/changepassword", SessionController, :change_password_submit

    get "/userprofile", UserController, :userprofile
    post "/userprofile", UserController, :userprofile_submit


    resources "/settings", SettingController , only: [:edit, :update, :new,:create]
    resources "/images", ImageGalleryController

    resources "/users", UserController
    resources "/webpages", WebpageController
    resources "/features", FeatureController
    resources "/featuresimage", FeatureImageController
    resources "/services", MedicalServiceController
    resources "/categories", CategoryController
    resources "/sub_categories", SubCategoryController
    resources "/contacts", ContactController
    resources "/addresses", ContactAddressController
    resources "/teams", TeamController


  end

  # Other scopes may use custom stacks.
  # scope "/api", BrickcroftWeb do
  #   pipe_through :api
  # end
end
