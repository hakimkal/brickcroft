defmodule BrickcroftWeb.Plugs.AdminLayout do


  import Phoenix.Controller, only: [ put_layout: 2 ]

  def init(_opts) , do: nil

  def call(conn, _) do

    conn
    |> put_layout({BrickcroftWeb.LayoutView, "admin.html"})

  end


end
