defmodule BrickcroftWeb.Plugs.LoadUser do

  import Plug.Conn

  alias Brickcroft.Administration

  def init(_opts) do
    nil
  end


  def call(conn, _opts) do

    user_id = get_session(conn, :user_id)

    if user_id != nil  && !conn.assigns[:current_user]  do


      user = user_id && Administration.get_user!(user_id)
      user
      |> Map.merge(%{password: nil, password_confirmation: nil, password_reset_code: nil, password_hash: nil})
      assign(conn, :current_user, user)

    else
      conn
    end
  end
end
