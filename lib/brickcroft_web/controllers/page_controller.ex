defmodule BrickcroftWeb.PageController do
  use BrickcroftWeb, :controller
  alias Brickcroft.Webpages
  alias BrickcroftWeb.CustompagesView
  alias Brickcroft.GeneralSettings
  alias Brickcroft.Gallery
  alias Brickcroft.MedicalServices
  alias Brickcroft.Teams
  alias Brickcroft.Features
  alias Brickcroft.Contacts
  alias Brickcroft.{Email, Mailer}


  plug BrickcroftWeb.Plugs.AuthenticateUser when action in [:dashboard]
  plug :add_settings when action in [:index, :show_by_slug]

  def index(conn, _params) do


    home_pics = Gallery.list_imagegalleries()

    features = Features.list_features()

    conn

    |> assign(:home_pics, home_pics)
    |> assign(:features, features)

    |> render("index.html")




  end

  def dashboard(conn, _params) do
    put_flash(conn, :info, "Welcome user")
    render(conn, "dashboard.html", layout: {BrickcroftWeb.LayoutView, "admin.html"})
  end



  def show_by_slug(conn, %{"slug" => slug}) do

    cond do
      slug == "features" ->


        webpage = Webpages.get_webpage_by_slug(slug)
        features = Features.list_features()
        conn
        |> put_view(CustompagesView)
        |> put_layout({BrickcroftWeb.LayoutView, "app.html"})

        |> render(
             "#{String.downcase(webpage.html_template)}.html",
             webpage: webpage,
             features: features
           )



      slug == "features" ->


        webpage = Webpages.get_webpage_by_slug(slug)
        features = Features.list_features()
        conn
        |> put_view(CustompagesView)
        |> put_layout({BrickcroftWeb.LayoutView, "app.html"})

        |> render(
             "#{String.downcase(webpage.html_template)}.html",
             webpage: webpage,
             features: features
           )


      slug == "portfolio" ->


        webpage = Webpages.get_webpage_by_slug(slug)
        images = Gallery.list_imagegalleries()
        conn
        |> put_view(CustompagesView)
        |> put_layout({BrickcroftWeb.LayoutView, "app.html"})

        |> render(
             "#{String.downcase(webpage.html_template)}.html",
             webpage: webpage,
             images: images
           )

      slug == "services" ->


        webpage = Webpages.get_webpage_by_slug(slug)

        conn
        |> put_view(CustompagesView)
        |> put_layout({BrickcroftWeb.LayoutView, "app.html"})

        |> render(
             "#{String.downcase(webpage.html_template)}.html",
             webpage: webpage
           )

      slug == "contact" ->


        webpage = Webpages.get_webpage_by_slug(slug)
        contacts = Contacts.list_contacts()
        conn
        |> put_view(CustompagesView)
        |> put_layout({BrickcroftWeb.LayoutView, "app.html"})

        |> render(
             "#{String.downcase(webpage.html_template)}.html",
             webpage: webpage,
             contacts: contacts
           )

      slug == "about" ->


        webpage = Webpages.get_webpage_by_slug(slug)
        #photos = Gallery.get_image_gallery_by_section("about")
        services = MedicalServices.list_medicalservices()


        conn
        |> put_view(CustompagesView)
        |> put_layout({BrickcroftWeb.LayoutView, "app.html"})

        |> render(
             "#{String.downcase(webpage.html_template)}.html",
             webpage: webpage,
             services: services
           )

      slug == "team" ->


        webpage = Webpages.get_webpage_by_slug(slug)

        team = Teams.list_teams()


        conn
        |> put_view(CustompagesView)
        |> put_layout({BrickcroftWeb.LayoutView, "app.html"})

        |> render(
             "#{String.downcase(webpage.html_template)}.html",
             webpage: webpage,
             teams: team
           )



      true ->

        webpage = Webpages.get_webpage_by_slug(slug)



        conn
        |> put_view(CustompagesView)
        |> put_layout({BrickcroftWeb.LayoutView, "app.html"})

        |> render(
             "#{String.downcase(webpage.html_template)}.html",
             webpage: webpage

           )

    end



    case  Webpages.get_webpage_by_slug(slug) do

      webpage when webpage != nil ->


        conn
        |> put_view(CustompagesView)
        |> put_layout({BrickcroftWeb.LayoutView, "app.html"})

        |> render(
             "#{String.downcase(webpage.html_template)}.html",
             webpage: webpage
           )
      _ ->
        conn
        |> redirect(to: Routes.page_path(conn, :index))
    end
  end


  def contact(conn, params) do


    email = params["email"]
    email2 = "al@imakandi.com"
    name = params["fullname"]
    subject = params["subject"]
    msg = params["message"]
    phone = params["phone"]


    case Recaptcha.verify(params["g-recaptcha-response"]) do
      {:ok, response} ->
        email2
        |> Email.send_html_email_from_contact(

             "Web2Email Message: #{subject}",
             "#{name}",
             email,
             msg,
             phone,
             subject
           )
        |> Mailer.deliver_later
        conn
        |> redirect(to: Routes.page_path(conn, :index))
      {:error, errors} ->
        conn
        |> redirect(to: Routes.page_path(conn, :show_by_slug, "contact"))
    end
  end


  defp add_settings(conn, _) do

    lst = GeneralSettings.list_generalsettings()

    if lst == [], do: conn
                      |> assign(:settings, nil)

    if lst != [], do: conn
                      |> assign(:settings, List.first(lst))

  end
end
