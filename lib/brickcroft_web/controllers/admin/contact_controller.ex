defmodule BrickcroftWeb.Admin.ContactController do
  use BrickcroftWeb, :controller

  alias Brickcroft.Contacts
  alias Brickcroft.Contacts.Contact

  def index(conn, _params) do
    contacts = Contacts.list_contacts()

    render(conn, "index.html", contacts: contacts)
  end

  def new(conn, _params) do
    changeset = Contacts.change_contact_assoc(%Contact{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"contact" => contact_params, "cads" => cads}) do
    case Contacts.create_contact(contact_params, cads) do
      {:ok, _contact} ->

        conn
        |> put_flash(:info, "Contact created successfully.")
        |> redirect(to: Routes.admin_contact_path(conn, :index))

      {:error, "Failed to create a contact"} ->
        conn
        |> put_flash(:info, "Failed to create a contact address")
        |> redirect(to: Routes.admin_contact_path(conn, :index))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    contact = Contacts.get_contact!(id)
    render(conn, "show.html", contact: contact)
  end

  def edit(conn, %{"id" => id}) do
    contact = Contacts.get_contact!(id)
    changeset = Contacts.change_contact_assoc(contact, contact.contact_address)
    render(conn, "edit.html", contact: contact, changeset: changeset)
  end

  def update(conn, %{"id" => id, "contact" => contact_params, "cads" => cads}) do
    contact = Contacts.get_contact!(id)

    case Contacts.update_contact(contact, contact_params, cads) do
      {:ok, _contact} ->

        conn
        |> put_flash(:info, "Contact updated successfully.")
        |> redirect(to: Routes.admin_contact_path(conn, :index))

      {:error, "Failed to create a contact"} ->
        conn
        |> put_flash(:info, "Failed to create a contact address")
        |> redirect(to: Routes.admin_contact_path(conn, :index))

      {:error, :rollback} ->
        conn
        |> put_flash(:info, "Failed to create a contact address")
        |> redirect(to: Routes.admin_contact_path(conn, :index))


      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contact: contact, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    contact = Contacts.get_contact!(id)
    {:ok, _contact} = Contacts.delete_contact(contact)

    conn
    |> put_flash(:info, "Contact deleted successfully.")
    |> redirect(to: Routes.admin_contact_path(conn, :index))
  end
end
