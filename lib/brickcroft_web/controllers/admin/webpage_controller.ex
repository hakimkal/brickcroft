defmodule BrickcroftWeb.Admin.WebpageController do
  use BrickcroftWeb, :controller

  alias Brickcroft.Webpages
  alias Brickcroft.Webpages.Webpage

  def index(conn, _params) do
    webpages = Webpages.list_title()
    render(conn, "index.html", webpages: webpages)
  end

  def new(conn, _params) do

    htmlfilenames = get_templates()


    changeset = Webpages.change_webpage(%Webpage{})
    render(conn, "new.html", changeset: changeset, files: htmlfilenames)
  end

  def create(conn, %{"webpage" => webpage_params}) do
    case Webpages.create_webpage(webpage_params) do
      {:ok, webpage} ->
        conn
        |> put_flash(:info, "Webpage created successfully.")
        |> redirect(to: Routes.admin_webpage_path(conn, :show, webpage))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    webpage = Webpages.get_webpage!(id)
    render(conn, "show.html", webpage: webpage)
  end

  def edit(conn, %{"id" => id}) do
    webpage = Webpages.get_webpage!(id)

    changeset = Webpages.change_webpage(webpage)
    render(conn, "edit.html", webpage: webpage, changeset: changeset,files: get_templates())
  end

  def update(conn, %{"id" => id, "webpage" => webpage_params}) do
    webpage = Webpages.get_webpage!(id)

    case Webpages.update_webpage(webpage, webpage_params) do
      {:ok, webpage} ->
        conn
        |> put_flash(:info, "Webpage updated successfully.")
        |> redirect(to: Routes.admin_webpage_path(conn, :show, webpage))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", webpage: webpage, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    webpage = Webpages.get_webpage!(id)
    {:ok, _webpage} = Webpages.delete_webpage(webpage)

    conn
    |> put_flash(:info, "Webpage deleted successfully.")
    |> redirect(to: Routes.admin_webpage_path(conn, :index))
  end


  defp get_templates() do

    #tpath = "lib/brickcroft_web/templates/custompages"
    {_,_ ,tpls }  = BrickcroftWeb.CustompagesView.__templates__
    tpls
    |> Enum.map(fn f -> List.first(String.split(f,".")) end )


     #File.ls!(tpath)
     #|> Enum.map( fn f -> String.upcase(Path.rootname(Path.rootname(f))) end)

  end
end
