defmodule BrickcroftWeb.Admin.SessionController do

  @moduledoc """
  The Session Controller.
  """
  use BrickcroftWeb, :controller
  alias Brickcroft.Administration
  alias Brickcroft.Email
  alias Brickcroft.Mailer

  plug :set_layout


  @doc """
   Returns login page for users.
  """

  def new(conn, _params) do
    if(is_logged_in(conn)) do
      conn
      |> redirect(to: Routes.page_path(conn, :dashboard))

    else
      conn
      |> render("new.html")

    end


  end

  @doc """
   Returns a successful login user and redirects to dashboard or the intending path.
  """
  def create(conn, %{"session" => session_params}) do

    case Administration.get_user_by_credentials(session_params) do
      :error ->
        conn
        |> put_flash(:error, "invalid username/password combination")
        |> render("new.html")

      user ->

        path = get_session(conn, :intending_to_visit) || Routes.page_path(conn, :dashboard)

        conn

        |> put_session(:user_id, user.id)
        |> configure_session(renew: true)
        |> put_flash(:info, "Login Successful")
        |> redirect(to: path)

    end

  end

  @doc """

  Clears session and log user out of the system
  """

  def delete(conn, _) do

    conn

    |> delete_session("current_user")
    |> delete_session(:user_id)

    |> configure_session(drop: true)


    |> put_flash(:info, "You have been logged out!")
    |> redirect(to: Routes.session_path(conn, :new))
  end


  @doc """
    forgot password form
  """

  def recover_password(conn, _) do

    conn
    |> render("forgot_password.html")


  end
  @doc """

  Generates password recovery code and send via email
  """


  def recover_password_submit(conn, %{"session" => session_params}) do

    conn
    |> clear_session()

    case  Administration.get_user_by_email(session_params["email"]) do

      :nil -> conn
              |> put_flash(:error, "There is no account with the provided email!")
              |> render("forgot_password.html")

      user ->

        {:ok, updated_user} = Administration.generate_password_reset_code(user)


        updated_user.email
        |> Email.send_html_email(

             "Password Reset link",
             "#{updated_user.firstname} #{updated_user.lastname}",
             "password_reset_email",
             updated_user.password_reset_code
           )
        |> Mailer.deliver_later
        conn
        |> put_flash(:info, "Password Recovery email has been sent to you. Please check your email.")
        |> redirect(to: Routes.page_path(conn, :dashboard))
    end




  end

  @doc """

  Returns password reset form to user with valid reset code
  """

  def reset_password(conn, %{"code" => code}) do

    if is_logged_in(conn) do

      conn
      |> put_flash(:info, "You have to be loggedOut to access the requested page!")
      |> redirect(to: Routes.session_path(conn, :new))

    end




    case  Administration.get_user_by_code(code) do


      user when user != nil ->


        conn
        |> assign(:user, user)
        |> assign(:code, code)
        |> render("reset_password.html")

      _ ->

        conn
        |> put_flash(:error, "Invalid password reset code, Please provide your email to receive code!")
        |> redirect(to: Routes.session_path(conn, :recover_password))
    end

  end

  @doc """

  Returns user to dashboard on valid reset code and new password updated
  """

  def reset_password_submit(conn, %{"code" => code, "session" => session_params}) do
    user = Administration.get_user_by_code(code)

    if is_nil(user)  do

      conn
      |> put_flash(:error, "User Not found!")
      |> redirect(to: Routes.page_path(conn, :index))
    end

    new_session_params = Enum.into(%{"password_reset_code" => nil}, session_params)


    case Administration.update_user(user, new_session_params) do

      {:ok, user} ->
        conn
        |> assign(:user, user)
        |> put_session(:user_id, user.id)
        |> configure_session(renew: true)
        |> put_flash(:info, "Login Successful")
        |> redirect(to: Routes.admin_user_path(conn, :index))

      {:error, _} ->
        conn
        |> put_flash(:error, "The operation failed")
        |> render("reset_password.html")


    end


  end


  def change_password(conn, _) do

    conn

    |> render("changepassword.html", layout: {BrickcroftWeb.LayoutView, "admin.html"})

  end

  def change_password_submit(conn, %{"session" => session_params}) do

    user = Administration.get_user!(get_session(conn, :user_id))
    case Administration.update_user(user, session_params) do

      {:ok, _changedpass} ->

        conn
        |> put_flash(:info, "Successfully changed password!")
        |> redirect(to: Routes.page_path(conn, :dashboard))

      {:error, changeset} ->
        IO.inspect(changeset)
        conn

        |> put_flash(:error, "Password change error!")
        |> assign(:errors, changeset.errors)
        |> render("changepassword.html", layout: {BrickcroftWeb.LayoutView, "admin.html"})

    end




  end

  defp set_layout(conn, _) do

    conn
    |> put_layout("admin_login.html")

  end


  defp is_logged_in(conn) do


    case conn.assigns[:current_user] do

      nil -> false
      _ -> true
    end



  end



end

