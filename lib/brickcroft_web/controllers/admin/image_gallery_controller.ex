defmodule BrickcroftWeb.Admin.ImageGalleryController do
  use BrickcroftWeb, :controller

  alias Brickcroft.{Gallery, Projects}
  alias Brickcroft.Gallery.ImageGallery

  plug :add_sections, only: [:new, :create, :edit, :update]

  @sections [:home, :about, :home_sales]

  #@uploadpath "/opt/brickcroftMEDIA/"

  def index(conn, _params) do
    imagegalleries = Gallery.list_imagegalleries()
    render(conn, "index.html", imagegalleries: imagegalleries)
  end

  def new(conn, _params) do
    changeset = Gallery.change_image_gallery(%ImageGallery{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"image_gallery" => image_gallery_params}) do



    if upload = image_gallery_params["photo"] do

      fpth = upload_file(upload)


      gallery_params = Map.merge(image_gallery_params, %{"image_path" => fpth})



      case Gallery.create_image_gallery(gallery_params) do
        {:ok, _image_gallery} ->
          conn
          |> put_flash(:info, "Image gallery created successfully.")
          |> redirect(to: Routes.admin_image_gallery_path(conn, :index))

        {:error, %Ecto.Changeset{} = changeset} ->
          delete_old_file(fpth)
          Enum.into(gallery_params, %{"image_path" => nil})
          render(conn, "new.html", changeset: changeset)
      end
    end
  end

  def show(conn, %{"id" => id}) do
    image_gallery = Gallery.get_image_gallery!(id)
    render(conn, "show.html", image_gallery: image_gallery)
  end

  def edit(conn, %{"id" => id}) do
    image_gallery = Gallery.get_image_gallery!(id)
    changeset = Gallery.change_image_gallery(image_gallery)
    render(conn, "edit.html", image_gallery: image_gallery, changeset: changeset)
  end

  def update(conn, %{"id" => id, "image_gallery" => image_gallery_params}) do
    image_gallery = Gallery.get_image_gallery!(id)
    if upload = image_gallery_params["photo"]  do

      fpth = upload_file(upload)
      delete_old_file(image_gallery.image_path)


      gpa  = Map.put(image_gallery_params, "image_path", fpth)


      case Gallery.update_image_gallery(image_gallery, gpa) do
        {:ok, image_gallery} ->
          conn
          |> put_flash(:info, "Image gallery updated successfully.")
          |> redirect(to: Routes.admin_image_gallery_path(conn, :show, image_gallery))

        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "edit.html", image_gallery: image_gallery, changeset: changeset)
      end

      else

      case Gallery.update_image_gallery(image_gallery, image_gallery_params) do
        {:ok, image_gallery} ->
          conn
          |> put_flash(:info, "Image gallery updated successfully.")
          |> redirect(to: Routes.admin_image_gallery_path(conn, :show, image_gallery))

        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "edit.html", image_gallery: image_gallery, changeset: changeset)
      end
    end

  end

  def delete(conn, %{"id" => id}) do
    image_gallery = Gallery.get_image_gallery!(id)

    delete_old_file(image_gallery.image_path)
    {:ok, _image_gallery} = Gallery.delete_image_gallery(image_gallery)

    conn
    |> put_flash(:info, "Image gallery deleted successfully.")
    |> redirect(to: Routes.admin_image_gallery_path(conn, :index))
  end





  defp add_sections(conn, _) do

    categories = Projects.list_categories()
                 |> retrieve_key_val
    subcategories = Projects.list_sub_categories()
                    |> retrieve_key_val
    conn
    |> assign(:sections, @sections)
    |> assign(:categories, categories)
    |> assign(:sub_categories, subcategories)


  end

  defp retrieve_key_val(list) do
    list
    |> Enum.map(&Map.take(&1, [:id, :name]))
    |> Enum.map(fn f -> {f.name, f.id} end)
  end
end
