defmodule BrickcroftWeb.Admin.MedicalServiceController do
  use BrickcroftWeb, :controller

  alias Brickcroft.MedicalServices
  alias Brickcroft.MedicalServices.MedicalService

  #contorller
  def index(conn, _params) do
    medicalservices = MedicalServices.list_medicalservices()
    render(conn, "index.html", medicalservices: medicalservices)
  end

  def new(conn, _params) do
    changeset = MedicalServices.build_medicalservice_assoc(%MedicalService{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"medical_service" => medical_service_params, "mdsp" => mdsp}) do


    case MedicalServices.create_medical_service(medical_service_params) do
      {:ok, medical_service} ->

        if mdsp do
          for {_j, jj} <- mdsp do


            mdsp
            |> Map.merge(%{"medical_service_id" => medical_service.id})
            |> MedicalServices.create_medical_service_pack()
          end
        end
        conn
        |> put_flash(:info, "service created successfully.")
        |> redirect(to: Routes.admin_medical_service_path(conn, :show, medical_service))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    medical_service = MedicalServices.get_medical_service_assoc!(id)

    render(conn, "show.html", medical_service: medical_service)
  end

  def edit(conn, %{"id" => id}) do
    medical_service = MedicalServices.get_medical_service_assoc!(id)
    changeset = MedicalServices.change_medical_service_assoc(medical_service)

    render(conn, "edit.html", medical_service: medical_service, changeset: changeset)
  end

  def update(conn, %{"id" => id, "medical_service" => medical_service_params, "mdsp" => mdsp}) do
    medical_service = MedicalServices.get_medical_service!(id)

    case MedicalServices.update_medical_service(medical_service, medical_service_params) do
      {:ok, medical_service} ->

        if mdsp do
          IO.inspect(mdsp)


            if(mdsp["package_detail"] != nil && mdsp["id"]  != nil && mdsp["package_detail"]  != "") do
              medical_service_pack = MedicalServices.get_medical_service_pack!(mdsp["id"])


              msp = Map.merge(mdsp, %{"medical_service_id" => medical_service.id})
              medical_service_pack
              |> MedicalServices.update_medical_service_pack(msp)


          end

          if(mdsp["package_detail"] != nil && mdsp["id"]  == nil && mdsp["package_detail"]  != "") do


            msp = Map.merge(mdsp, %{"medical_service_id" => medical_service.id})

            MedicalServices.create_medical_service_pack(msp)

          end
        end

        conn
        |> put_flash(:info, "service updated successfully.")
        |> redirect(to: Routes.admin_medical_service_path(conn, :show, medical_service))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", medical_service: medical_service, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    medical_service = MedicalServices.get_medical_service!(id)
    {:ok, _medical_service} = MedicalServices.delete_medical_service(medical_service)

    conn
    |> put_flash(:info, "service deleted successfully.")
    |> redirect(to: Routes.admin_medical_service_path(conn, :index))
  end
end
