defmodule BrickcroftWeb.Admin.SubCategoryController do
  use BrickcroftWeb, :controller

  alias Brickcroft.Projects
  alias Brickcroft.Projects.SubCategory

  def index(conn, _params) do
    sub_categories = Projects.list_sub_categories()
    render(conn, "index.html", sub_categories: sub_categories)
  end

  def new(conn, _params) do
    changeset = Projects.change_sub_category(%SubCategory{})
    categories = Projects.list_categories() |> retrieve_key_val
    render(conn, "new.html", changeset: changeset, categories: categories)
  end

  def create(conn, %{"sub_category" => sub_category_params}) do
    case Projects.create_sub_category(sub_category_params) do
      {:ok, sub_category} ->
        conn
        |> put_flash(:info, "Sub category created successfully.")
        |> redirect(to: Routes.admin_sub_category_path(conn, :show, sub_category))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    sub_category = Projects.get_sub_category!(id)
    render(conn, "show.html", sub_category: sub_category)
  end

  def edit(conn, %{"id" => id}) do
    sub_category = Projects.get_sub_category!(id)
    changeset = Projects.change_sub_category(sub_category)
    render(conn, "edit.html", sub_category: sub_category, changeset: changeset)
  end

  def update(conn, %{"id" => id, "sub_category" => sub_category_params}) do
    sub_category = Projects.get_sub_category!(id)

    case Projects.update_sub_category(sub_category, sub_category_params) do
      {:ok, sub_category} ->
        conn
        |> put_flash(:info, "Sub category updated successfully.")
        |> redirect(to: Routes.admin_sub_category_path(conn, :show, sub_category))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", sub_category: sub_category, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    sub_category = Projects.get_sub_category!(id)
    {:ok, _sub_category} = Projects.delete_sub_category(sub_category)

    conn
    |> put_flash(:info, "Sub category deleted successfully.")
    |> redirect(to: Routes.admin_sub_category_path(conn, :index))
  end

  defp retrieve_key_val(list) do
    list
    |> Enum.map(&Map.take(&1, [:id, :name]))
    |> Enum.map(fn f -> {f.name, f.id} end)
  end
end
