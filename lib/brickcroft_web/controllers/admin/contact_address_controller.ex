defmodule BrickcroftWeb.Admin.ContactAddressController do
  use BrickcroftWeb, :controller

  alias Brickcroft.Contacts
  alias Brickcroft.Contacts.ContactAddress

  def index(conn, _params) do
    contact_addresses = Contacts.list_contact_addresses()
    render(conn, "index.html", contact_addresses: contact_addresses)
  end

  def new(conn, _params) do
    changeset = Contacts.change_contact_address(%ContactAddress{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"contact_address" => contact_address_params}) do
    case Contacts.create_contact_address(contact_address_params) do
      {:ok, contact_address} ->
        conn
        |> put_flash(:info, "Contact address created successfully.")
        |> redirect(to: Routes.admin_contact_address_path(conn, :show, contact_address))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    contact_address = Contacts.get_contact_address!(id)
    render(conn, "show.html", contact_address: contact_address)
  end

  def edit(conn, %{"id" => id}) do
    contact_address = Contacts.get_contact_address!(id)
    changeset = Contacts.change_contact_address(contact_address)
    render(conn, "edit.html", contact_address: contact_address, changeset: changeset)
  end

  def update(conn, %{"id" => id, "contact_address" => contact_address_params}) do
    contact_address = Contacts.get_contact_address!(id)

    case Contacts.update_contact_address(contact_address, contact_address_params) do
      {:ok, contact_address} ->
        conn
        |> put_flash(:info, "Contact address updated successfully.")
        |> redirect(to: Routes.admin_contact_address_path(conn, :show, contact_address))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contact_address: contact_address, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    contact_address = Contacts.get_contact_address!(id)
    {:ok, _contact_address} = Contacts.delete_contact_address(contact_address)

    conn
    |> put_flash(:info, "Contact address deleted successfully.")
    |> redirect(to: Routes.admin_contact_address_path(conn, :index))
  end
end
