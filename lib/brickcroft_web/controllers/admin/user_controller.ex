defmodule BrickcroftWeb.Admin.UserController do
  use BrickcroftWeb, :controller

  alias Brickcroft.Administration
  alias Brickcroft.Administration.User

  def index(conn, _params) do
    users = Administration.list_users()
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Administration.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    new_user_params =  Enum.into(user_params, %{"password" => "123456", "password_confirmation" => "123456"})

    case Administration.create_user(new_user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User created successfully with a default password - `123456`")
        |> redirect(to: Routes.admin_user_path(conn, :index))


      {:error, %Ecto.Changeset{} = changeset} ->

        render(conn, "new.html", changeset: changeset)
    end
  end



  def edit(conn, %{"id" => id}) do
    user = Administration.get_user!(id)
    changeset = Administration.change_user(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Administration.get_user!(id)

    case Administration.update_user(user, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: Routes.admin_user_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Administration.get_user!(id)
    {:ok, _user} = Administration.delete_user(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: Routes.admin_user_path(conn, :index))
  end


  def userprofile(conn, _params) do

    user = Administration.get_user!(get_session(conn, :user_id))
    changeset = Administration.build_user(Map.from_struct(user))
    render(conn, "userprofile.html", changeset: changeset, user: user)
  end

  def userprofile_submit(conn, %{"user" => user_params}) do
    changeset = Administration.get_user!(get_session(conn, :user_id))
    case Administration.update_user(changeset, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "Successfully updated profile!")

        |> redirect(to: Routes.page_path(conn, :dashboard))

      {:error, u} ->
        conn
        |> assign(:changeset, u)
        |> render("userprofile.html")

    end

  end
end
