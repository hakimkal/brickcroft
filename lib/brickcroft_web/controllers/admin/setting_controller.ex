defmodule BrickcroftWeb.Admin.SettingController do
  use BrickcroftWeb, :controller

  alias Brickcroft.GeneralSettings
  alias Brickcroft.GeneralSettings.Setting



  def new(conn, _params) do
    setting = GeneralSettings.list_generalsettings()
    IO.inspect(setting)
    case setting do
      [] ->
        changeset = GeneralSettings.change_setting(%Setting{})
        render(conn, "new.html", changeset: changeset)
      [head | _] ->
        redirect(conn, to: Routes.admin_setting_path(conn, :edit, head.id))

    end


  end

  def create(conn, %{"setting" => setting_params}) do
    case GeneralSettings.create_setting(setting_params) do
      {:ok, _setting} ->
        conn
        |> put_flash(:info, "Setting created successfully.")
        |> redirect(to: Routes.session_path(conn, :new))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end



  def edit(conn, %{"id" => id}) do
    setting = GeneralSettings.get_setting!(id)
    case setting do
      nil ->
        changeset = GeneralSettings.change_setting(%Setting{})
        conn
        |> assign(:changeset, changeset)
        |> render("edit.html", setting: setting)
      _ ->
        changeset = GeneralSettings.change_setting(setting)


        conn
        |> assign(:changeset, changeset)
        |> render("edit.html", setting: setting)
    end


  end

  def update(conn, %{"id" => id, "setting" => setting_params}) do
    setting = GeneralSettings.get_setting!(id)

    case GeneralSettings.update_setting(setting, setting_params) do
      {:ok, _setting} ->
        conn
        |> put_flash(:info, "Setting updated successfully.")
        |> redirect(to: Routes.session_path(conn, :new))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", setting: setting, changeset: changeset)
    end
  end


end
