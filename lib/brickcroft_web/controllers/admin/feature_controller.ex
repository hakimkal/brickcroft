defmodule BrickcroftWeb.Admin.FeatureController do
  use BrickcroftWeb, :controller

  alias Brickcroft.Features
  alias Brickcroft.Features.Feature


  def index(conn, _params) do
    features = Features.list_features()
    render(conn, "index.html", features: features)
  end

  def new(conn, _params) do
    changeset = Features.change_feature_assoc(%Feature{})

    render(conn, "new.html", changeset: changeset, feature: [])
  end

  def create(conn, %{"feature" => feature_params, "fimage" => fimage}) do

    case Features.create_feature(feature_params) do
      {:ok, feature} ->

        if fimage do

          for {_j, jj} <- fimage do

            upl = upload_file(jj["photo"])

            fim = %{"image_path" => upl, "feature_id" => feature.id}
            Features.create_feature_image(fim)
          end
        end

        conn
        |> put_flash(:info, "Feature created successfully.")
        |> redirect(to: Routes.admin_feature_path(conn, :show, feature))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    feature = Features.get_feature!(id)
    render(conn, "show.html", feature: feature)
  end

  def edit(conn, %{"id" => id}) do
    feature = Features.get_feature!(id)
    changeset = Features.change_feature_assoc(feature)

    render(conn, "edit.html", feature: feature, changeset: changeset)
  end

  def update(conn, %{"id" => id, "feature" => feature_params, "fimage" => fimage}) do
    feature = Features.get_feature!(id)

    case Features.update_feature(feature, feature_params) do
      {:ok, feature} ->


        if fimage do

          for {_j, jj} <- fimage do

            upl = upload_file(jj["photo"])

            fim = %{"image_path" => upl, "feature_id" => feature.id}
            Features.create_feature_image(fim)
          end
        end
        conn
        |> put_flash(:info, "Feature updated successfully.")
        |> redirect(to: Routes.admin_feature_path(conn, :show, feature))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", feature: feature, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    feature = Features.get_feature!(id)
    if length(feature.feature_images) > 0 do
      feature.feature_images
      |> Enum.each(fn f -> delete_old_file(f.image_path) end)
    end
    {:ok, _feature} = Features.delete_feature(feature)

    conn
    |> put_flash(:info, "Feature deleted successfully.")
    |> redirect(to: Routes.admin_feature_path(conn, :index))
  end
end
