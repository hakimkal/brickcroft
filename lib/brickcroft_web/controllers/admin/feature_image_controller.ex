defmodule BrickcroftWeb.Admin.FeatureImageController do
  use BrickcroftWeb, :controller

  alias Brickcroft.Features
  alias Brickcroft.Features.FeatureImage

  def index(conn, _params) do
    e = Features.list_e()
    render(conn, "index.html", e: e)
  end

  def new(conn, _params) do
    changeset = Features.change_feature_image(%FeatureImage{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"feature_image" => feature_image_params}) do
    case Features.create_feature_image(feature_image_params) do
      {:ok, feature_image} ->
        conn
        |> put_flash(:info, "Feature image created successfully.")
        |> redirect(to: Routes.admin_feature_image_path(conn, :show, feature_image))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    feature_image = Features.get_feature_image!(id)
    render(conn, "show.html", feature_image: feature_image)
  end

  def edit(conn, %{"id" => id}) do
    feature_image = Features.get_feature_image!(id)
    changeset = Features.change_feature_image(feature_image)
    render(conn, "edit.html", feature_image: feature_image, changeset: changeset)
  end

  def update(conn, %{"id" => id, "feature_image" => feature_image_params}) do
    feature_image = Features.get_feature_image!(id)

    case Features.update_feature_image(feature_image, feature_image_params) do
      {:ok, feature_image} ->
        conn
        |> put_flash(:info, "Feature image updated successfully.")
        |> redirect(to: Routes.admin_feature_image_path(conn, :show, feature_image))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", feature_image: feature_image, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    feature_image = Features.get_feature_image!(id)
    delete_old_file(feature_image.image_path)
    {:ok, _feature_image} = Features.delete_feature_image(feature_image)

    conn
    |> put_flash(:info, "Feature image deleted successfully.")
    |> redirect(to: Routes.admin_feature_image_path(conn, :index))
  end
end
