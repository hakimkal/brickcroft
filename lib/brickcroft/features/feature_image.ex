defmodule Brickcroft.Features.FeatureImage do
  use Ecto.Schema
  import Ecto.Changeset
alias Brickcroft.Features.Feature

  schema "feature_images" do

    field :image_path, :string
    belongs_to :feature, Feature

    timestamps()
  end

  @doc false
  def changeset(feature_image, attrs) do
    feature_image
    |> cast(attrs, [ :image_path, :feature_id])
    |> validate_required([ :image_path, :feature_id])
  end
end
