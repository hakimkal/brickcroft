defmodule Brickcroft.Features do
  @moduledoc """
  The Features context.
  """

  import Ecto.Query, warn: false
  alias Brickcroft.Repo

  alias Brickcroft.Features.{Feature, FeatureImage}

  @doc """
  Returns the list of features.

  ## Examples

      iex> list_features()
      [%Feature{}, ...]

  """
  def list_features do
    Feature
    |> limit(2)
    |> Repo.all()
    |> Repo.preload(:feature_images)
  end

  @doc """
  Gets a single feature.

  Raises `Ecto.NoResultsError` if the Feature does not exist.

  ## Examples

      iex> get_feature!(123)
      %Feature{}

      iex> get_feature!(456)
      ** (Ecto.NoResultsError)

  """
  def get_feature!(id),
      do: Repo.get!(Feature, id)
          |> Repo.preload(:feature_images)

  @doc """
  Creates a feature.

  ## Examples

      iex> create_feature(%{field: value})
      {:ok, %Feature{}}

      iex> create_feature(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_feature(attrs \\ %{}) do
    %Feature{}
    |> Feature.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a feature.

  ## Examples

      iex> update_feature(feature, %{field: new_value})
      {:ok, %Feature{}}

      iex> update_feature(feature, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_feature(%Feature{} = feature, attrs) do
    feature
    |> Feature.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Feature.

  ## Examples

      iex> delete_feature(feature)
      {:ok, %Feature{}}

      iex> delete_feature(feature)
      {:error, %Ecto.Changeset{}}

  """
  def delete_feature(%Feature{} = feature) do
    Repo.delete(feature)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking feature changes.

  ## Examples

      iex> change_feature(feature)
      %Ecto.Changeset{source: %Feature{}}

  """
  def change_feature(%Feature{} = feature) do
    Feature.changeset(feature, %{})
  end

  def change_feature_assoc(%Feature{} = feature) do
    Feature.changeset_assoc(
      feature,
      %{
        feature_images: [
          Map.from_struct(%FeatureImage{}),
          Map.from_struct(%FeatureImage{}),
          Map.from_struct(%FeatureImage{}),
          Map.from_struct(%FeatureImage{}),
          Map.from_struct(%FeatureImage{})

        ]
      }
    )
  end


  alias Brickcroft.Features.FeatureImage

  @doc """
  Returns the list of e.

  ## Examples

      iex> list_e()
      [%FeatureImage{}, ...]

  """
  def list_e do
    Repo.all(FeatureImage)
    |> Repo.preload(:feature)
  end

  @doc """
  Gets a single feature_image.

  Raises `Ecto.NoResultsError` if the Feature image does not exist.

  ## Examples

      iex> get_feature_image!(123)
      %FeatureImage{}

      iex> get_feature_image!(456)
      ** (Ecto.NoResultsError)

  """
  def get_feature_image!(id), do: Repo.get!(FeatureImage, id)

  @doc """
  Creates a feature_image.

  ## Examples

      iex> create_feature_image(%{field: value})
      {:ok, %FeatureImage{}}

      iex> create_feature_image(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_feature_image(attrs \\ %{}) do
    %FeatureImage{}
    |> FeatureImage.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a feature_image.

  ## Examples

      iex> update_feature_image(feature_image, %{field: new_value})
      {:ok, %FeatureImage{}}

      iex> update_feature_image(feature_image, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_feature_image(%FeatureImage{} = feature_image, attrs) do
    feature_image
    |> FeatureImage.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a FeatureImage.

  ## Examples

      iex> delete_feature_image(feature_image)
      {:ok, %FeatureImage{}}

      iex> delete_feature_image(feature_image)
      {:error, %Ecto.Changeset{}}

  """
  def delete_feature_image(%FeatureImage{} = feature_image) do
    Repo.delete(feature_image)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking feature_image changes.

  ## Examples

      iex> change_feature_image(feature_image)
      %Ecto.Changeset{source: %FeatureImage{}}

  """
  def change_feature_image(%FeatureImage{} = feature_image) do
    FeatureImage.changeset(feature_image, %{})
  end
end
