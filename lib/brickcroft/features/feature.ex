defmodule Brickcroft.Features.Feature do
  use Ecto.Schema
  import Ecto.Changeset
  alias Brickcroft.Features.FeatureImage


  schema "features" do
    field :details, :string
    field :title, :string
    has_many  :feature_images, FeatureImage , on_replace: :nilify , on_delete: :delete_all
    timestamps()
  end

  @doc false
  def changeset(feature, attrs) do
    feature
    |> cast(attrs, [:title, :details])
    |> validate_required([:title, :details])
  end

  def changeset_assoc(feature,attrs) do
    feature
    |> changeset(attrs)
    |> cast_assoc(:feature_images)
  end
end
