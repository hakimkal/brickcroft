defmodule Brickcroft.Gallery.ImageGallery do
  use Ecto.Schema
  import Ecto.Changeset


  schema "imagegalleries" do
    field :image_path, :string
    field :section, :string
    field :subtitle, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(image_gallery, attrs) do
    image_gallery
    |> cast(attrs, [:image_path, :title, :subtitle, :section])
    |> validate_required([:image_path, :title])
  end
end
