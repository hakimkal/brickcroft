defmodule Brickcroft.Gallery do


  import Ecto.Query, warn: false
  alias Brickcroft.Repo

  alias Brickcroft.Gallery.ImageGallery


  def list_imagegalleries do

    Repo.all(ImageGallery)
    # |>Repo.preload(:category)

  end

  def get_image_gallery!(id) do
    Repo.get!(ImageGallery, id)

  end

  def get_image_gallery_by_section(section) do

    case igall = list_imagegalleries() do
      [] -> igall
      nil -> igall
      _ -> Enum.filter(igall, fn (f) -> f.section == section end)

    end

  end


  def create_image_gallery(attrs \\ %{}) do
    %ImageGallery{}
    |> ImageGallery.changeset(attrs)
    |> Repo.insert()
  end


  def update_image_gallery(%ImageGallery{} = image_gallery, attrs) do
    image_gallery
    |> ImageGallery.changeset(attrs)
    |> Repo.update()
  end


  def delete_image_gallery(%ImageGallery{} = image_gallery) do
    Repo.delete(image_gallery)
  end


  def change_image_gallery(%ImageGallery{} = image_gallery) do
    ImageGallery.changeset(image_gallery, %{})
  end
end
