defmodule Brickcroft.Email do
  @moduledoc false
  use Bamboo.Phoenix, view: BrickcroftWeb.EmailView





  def send_html_email_from_contact(email_address, subject, name, email,  msg, phone, thesubject) do
    email_address
    |> base_email(subject)
    |> assign(:name, name)
    |> bcc("info@leproghrammeen.com")
    |> cc(email)
    |> assign(:email, email)
    |> assign(:msg, msg)
    |> assign(:subject, thesubject)
    |> assign(:phone, phone)
    |> render(String.to_atom("web2email"))

  end



  def send_html_email(email_address, subject, name, template_name,pass_code) do
    email_address
    |> base_email(subject)
    |> assign(:name, name)
    |> assign(:email_address, email_address)
    |> assign(:password_reset_code, pass_code)
    |> render(String.to_atom("#{template_name}"))

  end

  defp base_email(email_address, subject) do
    # Here you can set a default from, default headers, etc.
    new_email()
    |> from("no-reply@brickcroftconstruction.com")
    |> to(email_address)
    |> subject(subject)
    |> put_html_layout({BrickcroftWeb.LayoutView, "email.html"})
    |> put_text_layout({BrickcroftWeb.LayoutView, "email.text"})
  end

end
