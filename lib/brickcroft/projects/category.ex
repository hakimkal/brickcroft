defmodule Brickcroft.Projects.Category do
  use Ecto.Schema
  import Ecto.Changeset

  alias Brickcroft.Projects.SubCategory
  schema "categories" do
    field :description, :string
    field :name, :string
    has_many :sub_category, SubCategory
    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:name, :description])
    |> validate_required([:name])
  end
end
