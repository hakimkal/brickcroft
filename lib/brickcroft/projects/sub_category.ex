defmodule Brickcroft.Projects.SubCategory do
  use Ecto.Schema
  import Ecto.Changeset
  alias Brickcroft.Projects.Category

  schema "sub_categories" do
    field :description, :string
    field :name, :string
    belongs_to :category, Category

    timestamps()
  end

  @doc false
  def changeset(sub_category, attrs) do
    sub_category
    |> cast(attrs, [:name, :description])
    |> validate_required([:name])
  end
end
