defmodule Brickcroft.Teams.Team do
  use Ecto.Schema
  import Ecto.Changeset


  schema "teams" do
    field :designation, :string
    field :name, :string
    field :profile, :string

    timestamps()
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [:name, :designation, :profile])
    |> validate_required([:name, :designation, :profile])
  end
end
