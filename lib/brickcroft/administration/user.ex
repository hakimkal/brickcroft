defmodule Brickcroft.Administration.User do
  use Ecto.Schema
  import Ecto.Changeset
  import Comeonin.Bcrypt, only: [hashpwsalt: 1]
  alias Brickcroft.Administration.User
  schema "users" do
    field :email, :string
    field :firstname, :string
    field :lastname, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    field :active, :boolean, default: true
    field :password_hash, :string
    field :phone, :string
    field :password_reset_code, :string

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:email, :firstname,:active, :password, :phone, :password_confirmation, :lastname, :password_reset_code])
    |> validate_required([:email, :firstname, :password, :password_confirmation, :phone, :lastname])
    |> validate_format(:email, ~r/@/, message: "is invalid")
    |> validate_length(:password, min: 6, max: 100)
    |> validate_confirmation(:password, message: "does not match password")
    |> unique_constraint(:email)
    |> put_hashed_password()
  end

  def passwordless_changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:email, :firstname, :phone, :lastname, :password_reset_code])
    |> validate_required([:email, :firstname, :phone, :lastname])
    |> validate_format(:email, ~r/@/, message: "is invalid")
    |> unique_constraint(:email)
  end






  defp put_hashed_password(changeset) do

    case changeset.valid?
      do
      true ->
        changes = changeset.changes
        put_change(changeset, :password_hash, hashpwsalt(changes.password))
      _ -> changeset
    end
  end

end
