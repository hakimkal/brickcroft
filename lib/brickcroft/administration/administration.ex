defmodule Brickcroft.Administration do


  @moduledoc """
  The Administration context.
  """

  import Ecto.Query, warn: false
  alias Brickcroft.Repo
  alias Brickcroft.Generator

  alias Brickcroft.Administration.User


  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """

  def build_user(attrs \\ %{}) do

    %User{}
    |> User.changeset(attrs)
  end

  def create_user(attrs) do
    attrs
    |> build_user
    |> Repo.insert

  end



  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do


    if attrs["password"] != nil  do
      user
      |> User.changeset(attrs)
      |> Repo.update()

    else

      user
      |> User.passwordless_changeset(attrs)
      |> Repo.update()
    end


  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    user
    |> User.changeset( attrs)
  end




  def generate_password_reset_code(%User{} = user) do
    reset_pin = Generator.randstring(32)
    update_user(user, %{password_reset_code: reset_pin})


  end


  def get_user_by_email(email) do
    Repo.get_by(User, email: email)

  end

  def get_user_by_code(code) do
    Repo.get_by(User,password_reset_code: code)

  end

  def get_user_by_credentials(%{"email" => email, "password" => pass}) do

    user = get_user_by_email(email)
    cond do
      user && Comeonin.Bcrypt.checkpw(pass, user.password_hash) -> user
      true -> :error
    end
  end



end
