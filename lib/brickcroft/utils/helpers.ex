defmodule Brickcroft.Helpers do
  import Plug.Conn
  import Phoenix.Controller
  @uploadpath "/opt/brickcroftMEDIA/"

  def render_blank(conn) do
    conn
    |> send_resp(204, "")
  end

  def render_error(conn, status, opts) do
    conn
    |> put_status(status)
    |> render(MyApp.ErrorView, "#{status}.json", opts)
  end

  def ensure_current_user(conn, _) do
    case conn.assigns.current_user do
      nil -> render_error(conn, 401, message: "unauthorized")
      _current_user -> conn
    end
  end

  def upload_file(upload) do

    extension = Path.extname(upload.filename)
   #

    fl = "#{:os.system_time()}-uploaded#{extension}"

    if File.exists?(fl) do
      IO.inspect(fl)
      File.rm!(fl)
    end
    File.cp(upload.path, "#{@uploadpath}#{fl}")
    fl

  end

  def delete_old_file(filename) do
    fl = "#{@uploadpath}#{filename}"

    if File.exists?(fl) do
      IO.inspect(fl)
      File.rm!(fl)
    end

  end

end
