defmodule Brickcroft.Webpages.Webpage do
  use Ecto.Schema
  import Ecto.Changeset


  schema "webpages" do
    field :title, :string
    field :content, :string
    field :html_template, :string
    field :slug, :string
    field :subtitle, :string

    timestamps()
  end

  @doc false
  def changeset(webpage, attrs) do
    webpage
    |> cast(attrs, [:title,:html_template, :content, :slug, :subtitle])
    |> validate_required([:title,:html_template, :content])
  end
end
