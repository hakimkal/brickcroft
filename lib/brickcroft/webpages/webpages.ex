defmodule Brickcroft.Webpages do

  import Ecto.Query, warn: false
  alias Brickcroft.Repo

  alias Brickcroft.Webpages.Webpage


  def list_title do
    Repo.all(Webpage)
  end


  def get_webpage!(id), do: Repo.get!(Webpage, id)


  def create_webpage(attrs \\ %{}) do
    %Webpage{}
    |> Webpage.changeset(attrs)
    |> Repo.insert()
  end


  def update_webpage(%Webpage{} = webpage, attrs) do
    webpage
    |> Webpage.changeset(attrs)
    |> Repo.update()
  end


  def delete_webpage(%Webpage{} = webpage) do
    Repo.delete(webpage)
  end

  def change_webpage(%Webpage{} = webpage) do
    Webpage.changeset(webpage, %{})
  end

  def get_webpage_by_slug(slug) do

    Repo.get_by(Webpage, slug: slug)
  end
end
