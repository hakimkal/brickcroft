defmodule Brickcroft.MedicalServices.MedicalService do
  use Ecto.Schema
  import Ecto.Changeset
alias Brickcroft.MedicalServices.MedicalServicePack

  schema "medicalservices" do
    field :details, :string
    field :title, :string
    has_one  :medicalservicepack, MedicalServicePack , on_replace: :nilify , on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(medical_service, attrs) do
    medical_service
    |> cast(attrs, [:title, :details])
    |> validate_required([:title])
  end

  def changeset_assoc(medical_service,attrs) do
    medical_service
    |> changeset(attrs)
    |> cast_assoc(:medicalservicepack)
  end
end
