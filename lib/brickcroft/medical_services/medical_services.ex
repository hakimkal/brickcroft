defmodule Brickcroft.MedicalServices do
  @moduledoc """
  The MedicalServices context.
  """

  import Ecto.Query, warn: false
  alias Brickcroft.Repo

  alias Brickcroft.MedicalServices.{MedicalService, MedicalServicePack}

  @doc """
  Returns the list of medicalservices.

  ## Examples

      iex> list_medicalservices()
      [%MedicalService{}, ...]

  """
  def list_medicalservices do
    MedicalService
    |> order_by([asc: :title])
    |> Repo.all()
    |> Repo.preload(:medicalservicepack)
  end

  @doc """
  Gets a single medical_service.

  Raises `Ecto.NoResultsError` if the Medical service does not exist.

  ## Examples

      iex> get_medical_service!(123)
      %MedicalService{}

      iex> get_medical_service!(456)
      ** (Ecto.NoResultsError)

  """
  def get_medical_service!(id), do: Repo.get!(MedicalService, id)
  def get_medical_service_pack!(id), do: Repo.get!(MedicalServicePack, id)
  def get_medical_service_assoc!(id),
      do: Repo.get!(MedicalService, id)
          |> Repo.preload(:medicalservicepack)

  @doc """
  Creates a medical_service.

  ## Examples

      iex> create_medical_service(%{field: value})
      {:ok, %MedicalService{}}

      iex> create_medical_service(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """

  def build_medicalservice(attrs \\ %{}) do
    %MedicalService{}
    |> MedicalService.changeset(attrs)
  end

  def build_medicalservicepack(attrs \\ %{}) do
    %MedicalServicePack{}
    |> MedicalServicePack.changeset(attrs)
  end


  def build_medicalservice_assoc(%MedicalService{} = medicalservice) do
    MedicalService.changeset_assoc(
      medicalservice,
      %{medicalservicepack: Map.from_struct(%MedicalServicePack{})}
    )
  end

  def create_medical_service(attrs \\ %{}) do
    build_medicalservice(attrs)
    |> Repo.insert()
  end


  def create_medical_service_pack(attrs \\ %{}) do
    build_medicalservicepack(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a medical_service.

  ## Examples

      iex> update_medical_service(medical_service, %{field: new_value})
      {:ok, %MedicalService{}}

      iex> update_medical_service(medical_service, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_medical_service(%MedicalService{} = medical_service, attrs) do
    medical_service
    |> MedicalService.changeset(attrs)
    |> Repo.update()
  end


  def update_medical_service_pack(%MedicalServicePack{} = medical_service_pack, attrs) do
    medical_service_pack
    |> MedicalServicePack.changeset(attrs)
    |> Repo.update()
  end
  @doc """
  Deletes a MedicalService.

  ## Examples

      iex> delete_medical_service(medical_service)
      {:ok, %MedicalService{}}

      iex> delete_medical_service(medical_service)
      {:error, %Ecto.Changeset{}}

  """
  def delete_medical_service(%MedicalService{} = medical_service) do
    Repo.delete(medical_service)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking medical_service changes.

  ## Examples

      iex> change_medical_service(medical_service)
      %Ecto.Changeset{source: %MedicalService{}}

  """
  def change_medical_service(%MedicalService{} = medical_service) do
    MedicalService.changeset(medical_service, %{})
  end

  def change_medical_service_assoc(%MedicalService{} = medical_service) do
    MedicalService.changeset_assoc(medical_service, %{})
  end
end
