defmodule Brickcroft.MedicalServices.MedicalServicePack do
  use Ecto.Schema
  import Ecto.Changeset

  alias Brickcroft.MedicalServices.MedicalService

  schema "medicalservicepacks" do
    field :package_detail, :string
    field :medical_service_id, :id
    #belongs_to :medicalservice, MedicalService

    timestamps()
  end

  @doc false
  def changeset(medical_service_pack, attrs) do
    medical_service_pack
    |> cast(attrs, [:package_detail, :medical_service_id])
   #|> validate_required([:package_detail])
  end
end
