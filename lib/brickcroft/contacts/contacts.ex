defmodule Brickcroft.Contacts do
  @moduledoc """
  The Contacts context.
  """

  import Ecto.Query, warn: false
  alias Brickcroft.Repo

  alias Brickcroft.Contacts.{Contact, ContactAddress}

  @doc """
  Returns the list of contacts.

  ## Examples

      iex> list_contacts()
      [%Contact{}, ...]

  """
  def list_contacts do
    Repo.all(Contact)
    |> Repo.preload(:contact_address)
  end

  @doc """
  Gets a single contact.

  Raises `Ecto.NoResultsError` if the Contact does not exist.

  ## Examples

      iex> get_contact!(123)
      %Contact{}

      iex> get_contact!(456)
      ** (Ecto.NoResultsError)

  """
  def get_contact!(id),
      do: Repo.get!(Contact, id)
          |> Repo.preload(:contact_address)

  @doc """
  Creates a contact.

  ## Examples

      iex> create_contact(%{field: value})
      {:ok, %Contact{}}

      iex> create_contact(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_contact(attrs, cads) do

    Repo.transaction(
      fn ->
        with {:ok, contact} <-

               %Contact{}
               |> Contact.changeset(attrs)
               |> Repo.insert(),

             :ok <- create_addresses(contact, cads)
          do
          contact

        else
          _ ->
            Repo.rollback("Failed to create a contact")
        end

      end
    )

  end

  @doc """
  Updates a contact.

  ## Examples

      iex> update_contact(contact, %{field: new_value})
      {:ok, %Contact{}}

      iex> update_contact(contact, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_contact(%Contact{} = contact, attrs, cads) do


    Repo.transaction(
      fn ->

        with {:ok, contact} <-
               contact
               |> Contact.changeset(attrs)
               |> Repo.update(),

             :ok <- create_addresses(contact, cads)

          do
          contact

        else
          _ ->
            Repo.rollback("Failed to create a contact")
        end

      end
    )



  end

  def create_addresses(contact, cads) do

    cads
    |> Enum.each(
         fn {_x, f} ->

           ff = Map.merge(f, %{"contact_id" => contact.id})

           create_contact_address(ff)


         end
       )



  end

  @doc """
  Deletes a Contact.

  ## Examples

      iex> delete_contact(contact)
      {:ok, %Contact{}}

      iex> delete_contact(contact)
      {:error, %Ecto.Changeset{}}

  """
  def delete_contact(%Contact{} = contact) do
    Repo.delete(contact)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking contact changes.

  ## Examples

      iex> change_contact(contact)
      %Ecto.Changeset{source: %Contact{}}

  """
  def change_contact(%Contact{} = contact) do
    Contact.changeset(contact, %{})
  end

  def change_contact_assoc(%Contact{} = contact, contact_addresses \\ []) do

    case contact_addresses do

      [] -> Contact.changeset_assoc(
              contact,
              %{
                contact_address: [
                  Map.from_struct(%ContactAddress{}),


                ]
              }
            )
      _ ->
        Contact.changeset_assoc(
          contact,
          %{
            contact_address:
              Map.from_struct(Map.drop(List.first(contact_addresses), [:__meta__])),


          }
        )
    end
  end

  alias Brickcroft.Contacts.ContactAddress

  @doc """
  Returns the list of contact_addresses.

  ## Examples

      iex> list_contact_addresses()
      [%ContactAddress{}, ...]

  """
  def list_contact_addresses do
    Repo.all(ContactAddress)
    |> Repo.preload(:contact)
  end

  @doc """
  Gets a single contact_address.

  Raises `Ecto.NoResultsError` if the Contact address does not exist.

  ## Examples

      iex> get_contact_address!(123)
      %ContactAddress{}

      iex> get_contact_address!(456)
      ** (Ecto.NoResultsError)

  """
  def get_contact_address!(id),
      do: Repo.get!(ContactAddress, id)
          |> Repo.preload(:contact)

  @doc """
  Creates a contact_address.

  ## Examples

      iex> create_contact_address(%{field: value})
      {:ok, %ContactAddress{}}

      iex> create_contact_address(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_contact_address(attrs \\ %{}) do

    if attrs["id"] do
      case Repo.get(ContactAddress, attrs["id"]) do
        contact_address -> contact_address

      end
      else
       %ContactAddress{}
    end

    |> ContactAddress.changeset(attrs)
    |> Repo.insert_or_update()
  end

  @doc """
  Updates a contact_address.

  ## Examples

      iex> update_contact_address(contact_address, %{field: new_value})
      {:ok, %ContactAddress{}}

      iex> update_contact_address(contact_address, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_contact_address(%ContactAddress{} = contact_address, attrs) do
    contact_address
    |> ContactAddress.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a ContactAddress.

  ## Examples

      iex> delete_contact_address(contact_address)
      {:ok, %ContactAddress{}}

      iex> delete_contact_address(contact_address)
      {:error, %Ecto.Changeset{}}

  """
  def delete_contact_address(%ContactAddress{} = contact_address) do
    Repo.delete(contact_address)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking contact_address changes.

  ## Examples

      iex> change_contact_address(contact_address)
      %Ecto.Changeset{source: %ContactAddress{}}

  """
  def change_contact_address(%ContactAddress{} = contact_address) do
    ContactAddress.changeset(contact_address, %{})
  end
end
