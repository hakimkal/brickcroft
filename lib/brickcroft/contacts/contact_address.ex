defmodule Brickcroft.Contacts.ContactAddress do
  use Ecto.Schema
  import Ecto.Changeset
  alias Brickcroft.Contacts.Contact

  schema "contact_addresses" do
    field :address, :string
    field :email, :string
    field :phone, :string
    belongs_to :contact, Contact
    timestamps()
  end

  @doc false
  def changeset(contact_address, attrs) do
    contact_address
    |> cast(attrs, [:address, :email, :contact_id, :phone])

    |> validate_required([:address, :email, :phone,:contact_id])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email,[name: :contact_addresses_address_email_index,message: "already exist"])
    |> unique_constraint(:phone,[name: :contact_addresses_address_email_index])
    |> unique_constraint(:address, [name: :contact_addresses_address_email_index])
  end
end
