defmodule Brickcroft.Contacts.Contact do
  use Ecto.Schema
  import Ecto.Changeset
  alias Brickcroft.Contacts.ContactAddress

  schema "contacts" do
    field :location, :string
    has_many :contact_address, ContactAddress , on_delete: :delete_all
    timestamps()
  end

  @doc false
  def changeset(contact, attrs) do
    contact
    |> cast(attrs, [:location])

    |> validate_required([:location])
    |> unique_constraint(:location)
  end



  def changeset_assoc(contact, attrs) do
    contact
    |> changeset(attrs)
    |> cast_assoc(:contact_address)

  end
end
