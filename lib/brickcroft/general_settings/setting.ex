defmodule Brickcroft.GeneralSettings.Setting do
  use Ecto.Schema
  import Ecto.Changeset


  schema "generalsettings" do

    field :mission, :string
    field :vision, :string
    field :values, :string
    field :footer_brief_pitch ,:string
    field :footer_brief_service, :string
    field :header_address, :string
    field :header_phones, :string
    field :header_email, :string

    timestamps()
  end

  @doc false
  def changeset(setting, attrs) do
    setting
    |> cast(attrs, [:mission, :vision, :values, :header_address,:header_phones,:header_email,:footer_brief_service, :footer_brief_pitch])
    |> validate_required([:mission, :vision,:values,  :header_address,:header_phones,:header_email,:footer_brief_service, :footer_brief_pitch])
  end
end
