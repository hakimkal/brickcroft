defmodule Brickcroft.Repo do
  use Ecto.Repo,
    otp_app: :brickcroft,
    adapter: Ecto.Adapters.Postgres
end
