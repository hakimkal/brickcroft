#!/binbash

set -e
CURRENT_NAME="Rsh"
CURRENT_OTP="rsh"
NEW_NAME="Brickcroft"
NEW_OTP="brickcroft"



ack -l $CURRENT_NAME | xargs sed -i '' -e "s/$CURRENT_NAME/$NEW_NAME/g"
ack -l $CURRENT_OTP | xargs sed -i '' -e "s/$CURRENT_OTP/$NEW_OTP/g"

mv lib/$CURRENT_OTP lib/$NEW_OTP
mv lib/$CURRENT_OTP.ex lib/$NEW_OTP.ex

#Whoever uses this script will replace the names for their own use case
