defmodule Brickcroft.WebpagesTest do
  use Brickcroft.DataCase

  alias Brickcroft.Webpages

  describe "title" do
    alias Brickcroft.Webpages.Webpage

    @valid_attrs %{content: "some content", html_template: "some html_template", slug: "some slug", subtitle: "some subtitle"}
    @update_attrs %{content: "some updated content", html_template: "some updated html_template", slug: "some updated slug", subtitle: "some updated subtitle"}
    @invalid_attrs %{content: nil, html_template: nil, slug: nil, subtitle: nil}

    def webpage_fixture(attrs \\ %{}) do
      {:ok, webpage} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Webpages.create_webpage()

      webpage
    end

    test "list_title/0 returns all title" do
      webpage = webpage_fixture()
      assert Webpages.list_title() == [webpage]
    end

    test "get_webpage!/1 returns the webpage with given id" do
      webpage = webpage_fixture()
      assert Webpages.get_webpage!(webpage.id) == webpage
    end

    test "create_webpage/1 with valid data creates a webpage" do
      assert {:ok, %Webpage{} = webpage} = Webpages.create_webpage(@valid_attrs)
      assert webpage.content == "some content"
      assert webpage.html_template == "some html_template"
      assert webpage.slug == "some slug"
      assert webpage.subtitle == "some subtitle"
    end

    test "create_webpage/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Webpages.create_webpage(@invalid_attrs)
    end

    test "update_webpage/2 with valid data updates the webpage" do
      webpage = webpage_fixture()
      assert {:ok, %Webpage{} = webpage} = Webpages.update_webpage(webpage, @update_attrs)
      assert webpage.content == "some updated content"
      assert webpage.html_template == "some updated html_template"
      assert webpage.slug == "some updated slug"
      assert webpage.subtitle == "some updated subtitle"
    end

    test "update_webpage/2 with invalid data returns error changeset" do
      webpage = webpage_fixture()
      assert {:error, %Ecto.Changeset{}} = Webpages.update_webpage(webpage, @invalid_attrs)
      assert webpage == Webpages.get_webpage!(webpage.id)
    end

    test "delete_webpage/1 deletes the webpage" do
      webpage = webpage_fixture()
      assert {:ok, %Webpage{}} = Webpages.delete_webpage(webpage)
      assert_raise Ecto.NoResultsError, fn -> Webpages.get_webpage!(webpage.id) end
    end

    test "change_webpage/1 returns a webpage changeset" do
      webpage = webpage_fixture()
      assert %Ecto.Changeset{} = Webpages.change_webpage(webpage)
    end
  end
end
