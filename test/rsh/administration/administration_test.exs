defmodule Brickcroft.AdministrationTest do
  use Brickcroft.DataCase

  alias Brickcroft.Administration
  alias Brickcroft.Repo

  describe "users" do
    alias Brickcroft.Administration.User

    @valid_attrs %{email: "hakim@gm.com", firstname: "Abdul", lastname: " Hakim", password: "123456", phone: "08023456780"}
    @update_attrs %{email: "hakim@gm2.com", firstname: "Abdulhakim", lastname: "Khalib"}
    @invalid_attrs %{email: nil, firstname: nil, lastname: nil}

    def user_fixture() do

      import Comeonin.Bcrypt , only: [hashpwsalt: 1]
      pswd = hashpwsalt("123456")
      user  =  %User{ email: "hakim@g.com", firstname: "Abdul", lastname: " Hakim" ,password_hash: pswd , phone: "08023456780"}


      {:ok, new_user } = Repo.insert user
      new_user

    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Administration.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Administration.get_user!(user.id).email == user.email
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Administration.create_user(@valid_attrs)
      assert user.email == "hakim@gm.com"
      assert user.firstname == "Abdul"
      assert user.lastname == " Hakim"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Administration.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Administration.update_user(user, @update_attrs)
      assert user.email == @update_attrs.email
      assert user.firstname == @update_attrs.firstname
      assert user.lastname == @update_attrs.lastname
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Administration.update_user(user, @invalid_attrs)
      assert user.email == Administration.get_user!(user.id).email
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Administration.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Administration.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Administration.change_user(user)
    end

    test "generate_password_code/1 updates the  user " do
      user = user_fixture()
      assert user.password_reset_code == nil
      {:ok,new_user} = Administration.generate_password_reset_code(user)

      assert new_user.password_reset_code != nil
    end
  end
end
