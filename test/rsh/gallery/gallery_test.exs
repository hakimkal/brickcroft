defmodule Brickcroft.GalleryTest do
  use Brickcroft.DataCase

  alias Brickcroft.Gallery

  describe "imagegalleries" do
    alias Brickcroft.Gallery.ImageGallery

    @valid_attrs %{image_path: "some image_path", section: "some section", subtitle: "some subtitle", title: "some title"}
    @update_attrs %{image_path: "some updated image_path", section: "some updated section", subtitle: "some updated subtitle", title: "some updated title"}
    @invalid_attrs %{image_path: nil, section: nil, subtitle: nil, title: nil}

    def image_gallery_fixture(attrs \\ %{}) do
      {:ok, image_gallery} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Gallery.create_image_gallery()

      image_gallery
    end

    test "list_imagegalleries/0 returns all imagegalleries" do
      image_gallery = image_gallery_fixture()
      assert Gallery.list_imagegalleries() == [image_gallery]
    end

    test "get_image_gallery!/1 returns the image_gallery with given id" do
      image_gallery = image_gallery_fixture()
      assert Gallery.get_image_gallery!(image_gallery.id) == image_gallery
    end

    test "create_image_gallery/1 with valid data creates a image_gallery" do
      assert {:ok, %ImageGallery{} = image_gallery} = Gallery.create_image_gallery(@valid_attrs)
      assert image_gallery.image_path == "some image_path"
      assert image_gallery.section == "some section"
      assert image_gallery.subtitle == "some subtitle"
      assert image_gallery.title == "some title"
    end

    test "create_image_gallery/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Gallery.create_image_gallery(@invalid_attrs)
    end

    test "update_image_gallery/2 with valid data updates the image_gallery" do
      image_gallery = image_gallery_fixture()
      assert {:ok, %ImageGallery{} = image_gallery} = Gallery.update_image_gallery(image_gallery, @update_attrs)
      assert image_gallery.image_path == "some updated image_path"
      assert image_gallery.section == "some updated section"
      assert image_gallery.subtitle == "some updated subtitle"
      assert image_gallery.title == "some updated title"
    end

    test "update_image_gallery/2 with invalid data returns error changeset" do
      image_gallery = image_gallery_fixture()
      assert {:error, %Ecto.Changeset{}} = Gallery.update_image_gallery(image_gallery, @invalid_attrs)
      assert image_gallery == Gallery.get_image_gallery!(image_gallery.id)
    end

    test "delete_image_gallery/1 deletes the image_gallery" do
      image_gallery = image_gallery_fixture()
      assert {:ok, %ImageGallery{}} = Gallery.delete_image_gallery(image_gallery)
      assert_raise Ecto.NoResultsError, fn -> Gallery.get_image_gallery!(image_gallery.id) end
    end

    test "change_image_gallery/1 returns a image_gallery changeset" do
      image_gallery = image_gallery_fixture()
      assert %Ecto.Changeset{} = Gallery.change_image_gallery(image_gallery)
    end
  end
end
