defmodule Brickcroft.MedicalServicesTest do
  use Brickcroft.DataCase

  alias Brickcroft.MedicalServices

  describe "title" do
    alias Brickcroft.MedicalServices.MedicalService

    @valid_attrs %{details: "some details"}
    @update_attrs %{details: "some updated details"}
    @invalid_attrs %{details: nil}

    def medical_service_fixture(attrs \\ %{}) do
      {:ok, medical_service} =
        attrs
        |> Enum.into(@valid_attrs)
        |> MedicalServices.create_medical_service()

      medical_service
    end

    test "list_title/0 returns all title" do
      medical_service = medical_service_fixture()
      assert MedicalServices.list_title() == [medical_service]
    end

    test "get_medical_service!/1 returns the medical_service with given id" do
      medical_service = medical_service_fixture()
      assert MedicalServices.get_medical_service!(medical_service.id) == medical_service
    end

    test "create_medical_service/1 with valid data creates a medical_service" do
      assert {:ok, %MedicalService{} = medical_service} = MedicalServices.create_medical_service(@valid_attrs)
      assert medical_service.details == "some details"
    end

    test "create_medical_service/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = MedicalServices.create_medical_service(@invalid_attrs)
    end

    test "update_medical_service/2 with valid data updates the medical_service" do
      medical_service = medical_service_fixture()
      assert {:ok, %MedicalService{} = medical_service} = MedicalServices.update_medical_service(medical_service, @update_attrs)
      assert medical_service.details == "some updated details"
    end

    test "update_medical_service/2 with invalid data returns error changeset" do
      medical_service = medical_service_fixture()
      assert {:error, %Ecto.Changeset{}} = MedicalServices.update_medical_service(medical_service, @invalid_attrs)
      assert medical_service == MedicalServices.get_medical_service!(medical_service.id)
    end

    test "delete_medical_service/1 deletes the medical_service" do
      medical_service = medical_service_fixture()
      assert {:ok, %MedicalService{}} = MedicalServices.delete_medical_service(medical_service)
      assert_raise Ecto.NoResultsError, fn -> MedicalServices.get_medical_service!(medical_service.id) end
    end

    test "change_medical_service/1 returns a medical_service changeset" do
      medical_service = medical_service_fixture()
      assert %Ecto.Changeset{} = MedicalServices.change_medical_service(medical_service)
    end
  end

  describe "medicalservices" do
    alias Brickcroft.MedicalServices.MedicalService

    @valid_attrs %{details: "some details", title: "some title"}
    @update_attrs %{details: "some updated details", title: "some updated title"}
    @invalid_attrs %{details: nil, title: nil}

    def medical_service_fixture(attrs \\ %{}) do
      {:ok, medical_service} =
        attrs
        |> Enum.into(@valid_attrs)
        |> MedicalServices.create_medical_service()

      medical_service
    end

    test "list_medicalservices/0 returns all medicalservices" do
      medical_service = medical_service_fixture()
      assert MedicalServices.list_medicalservices() == [medical_service]
    end

    test "get_medical_service!/1 returns the medical_service with given id" do
      medical_service = medical_service_fixture()
      assert MedicalServices.get_medical_service!(medical_service.id) == medical_service
    end

    test "create_medical_service/1 with valid data creates a medical_service" do
      assert {:ok, %MedicalService{} = medical_service} = MedicalServices.create_medical_service(@valid_attrs)
      assert medical_service.details == "some details"
      assert medical_service.title == "some title"
    end

    test "create_medical_service/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = MedicalServices.create_medical_service(@invalid_attrs)
    end

    test "update_medical_service/2 with valid data updates the medical_service" do
      medical_service = medical_service_fixture()
      assert {:ok, %MedicalService{} = medical_service} = MedicalServices.update_medical_service(medical_service, @update_attrs)
      assert medical_service.details == "some updated details"
      assert medical_service.title == "some updated title"
    end

    test "update_medical_service/2 with invalid data returns error changeset" do
      medical_service = medical_service_fixture()
      assert {:error, %Ecto.Changeset{}} = MedicalServices.update_medical_service(medical_service, @invalid_attrs)
      assert medical_service == MedicalServices.get_medical_service!(medical_service.id)
    end

    test "delete_medical_service/1 deletes the medical_service" do
      medical_service = medical_service_fixture()
      assert {:ok, %MedicalService{}} = MedicalServices.delete_medical_service(medical_service)
      assert_raise Ecto.NoResultsError, fn -> MedicalServices.get_medical_service!(medical_service.id) end
    end

    test "change_medical_service/1 returns a medical_service changeset" do
      medical_service = medical_service_fixture()
      assert %Ecto.Changeset{} = MedicalServices.change_medical_service(medical_service)
    end
  end
end
