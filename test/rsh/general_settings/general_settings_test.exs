defmodule Brickcroft.GeneralSettingsTest do
  use Brickcroft.DataCase

  alias Brickcroft.GeneralSettings

  describe "generalsettings" do
    alias Brickcroft.GeneralSettings.Setting

    @valid_attrs %{mdnote: "some mdnote", mission: "some mission", sales_caption: "some sales_caption", sales_note: "some sales_note", vision: "some vision"}
    @update_attrs %{mdnote: "some updated mdnote", mission: "some updated mission", sales_caption: "some updated sales_caption", sales_note: "some updated sales_note", vision: "some updated vision"}
    @invalid_attrs %{mdnote: nil, mission: nil, sales_caption: nil, sales_note: nil, vision: nil}

    def setting_fixture(attrs \\ %{}) do
      {:ok, setting} =
        attrs
        |> Enum.into(@valid_attrs)
        |> GeneralSettings.create_setting()

      setting
    end

    test "list_generalsettings/0 returns all generalsettings" do
      setting = setting_fixture()
      assert GeneralSettings.list_generalsettings() == [setting]
    end

    test "get_setting!/1 returns the setting with given id" do
      setting = setting_fixture()
      assert GeneralSettings.get_setting!(setting.id) == setting
    end

    test "create_setting/1 with valid data creates a setting" do
      assert {:ok, %Setting{} = setting} = GeneralSettings.create_setting(@valid_attrs)
      assert setting.mdnote == "some mdnote"
      assert setting.mission == "some mission"
      assert setting.sales_caption == "some sales_caption"
      assert setting.sales_note == "some sales_note"
      assert setting.vision == "some vision"
    end

    test "create_setting/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = GeneralSettings.create_setting(@invalid_attrs)
    end

    test "update_setting/2 with valid data updates the setting" do
      setting = setting_fixture()
      assert {:ok, %Setting{} = setting} = GeneralSettings.update_setting(setting, @update_attrs)
      assert setting.mdnote == "some updated mdnote"
      assert setting.mission == "some updated mission"
      assert setting.sales_caption == "some updated sales_caption"
      assert setting.sales_note == "some updated sales_note"
      assert setting.vision == "some updated vision"
    end

    test "update_setting/2 with invalid data returns error changeset" do
      setting = setting_fixture()
      assert {:error, %Ecto.Changeset{}} = GeneralSettings.update_setting(setting, @invalid_attrs)
      assert setting == GeneralSettings.get_setting!(setting.id)
    end

    test "delete_setting/1 deletes the setting" do
      setting = setting_fixture()
      assert {:ok, %Setting{}} = GeneralSettings.delete_setting(setting)
      assert_raise Ecto.NoResultsError, fn -> GeneralSettings.get_setting!(setting.id) end
    end

    test "change_setting/1 returns a setting changeset" do
      setting = setting_fixture()
      assert %Ecto.Changeset{} = GeneralSettings.change_setting(setting)
    end
  end
end
