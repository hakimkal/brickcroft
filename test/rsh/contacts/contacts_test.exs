defmodule Brickcroft.ContactsTest do
  use Brickcroft.DataCase

  alias Brickcroft.Contacts

  describe "contacts" do
    alias Brickcroft.Contacts.Contact

    @valid_attrs %{location: "some location"}
    @update_attrs %{location: "some updated location"}
    @invalid_attrs %{location: nil}

    def contact_fixture(attrs \\ %{}) do
      {:ok, contact} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Contacts.create_contact()

      contact
    end

    test "list_contacts/0 returns all contacts" do
      contact = contact_fixture()
      assert Contacts.list_contacts() == [contact]
    end

    test "get_contact!/1 returns the contact with given id" do
      contact = contact_fixture()
      assert Contacts.get_contact!(contact.id) == contact
    end

    test "create_contact/1 with valid data creates a contact" do
      assert {:ok, %Contact{} = contact} = Contacts.create_contact(@valid_attrs)
      assert contact.location == "some location"
    end

    test "create_contact/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Contacts.create_contact(@invalid_attrs)
    end

    test "update_contact/2 with valid data updates the contact" do
      contact = contact_fixture()
      assert {:ok, %Contact{} = contact} = Contacts.update_contact(contact, @update_attrs)
      assert contact.location == "some updated location"
    end

    test "update_contact/2 with invalid data returns error changeset" do
      contact = contact_fixture()
      assert {:error, %Ecto.Changeset{}} = Contacts.update_contact(contact, @invalid_attrs)
      assert contact == Contacts.get_contact!(contact.id)
    end

    test "delete_contact/1 deletes the contact" do
      contact = contact_fixture()
      assert {:ok, %Contact{}} = Contacts.delete_contact(contact)
      assert_raise Ecto.NoResultsError, fn -> Contacts.get_contact!(contact.id) end
    end

    test "change_contact/1 returns a contact changeset" do
      contact = contact_fixture()
      assert %Ecto.Changeset{} = Contacts.change_contact(contact)
    end
  end

  describe "contact_addresses" do
    alias Brickcroft.Contacts.ContactAddress

    @valid_attrs %{address: "some address", email: "some email", phone: "some phone"}
    @update_attrs %{address: "some updated address", email: "some updated email", phone: "some updated phone"}
    @invalid_attrs %{address: nil, email: nil, phone: nil}

    def contact_address_fixture(attrs \\ %{}) do
      {:ok, contact_address} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Contacts.create_contact_address()

      contact_address
    end

    test "list_contact_addresses/0 returns all contact_addresses" do
      contact_address = contact_address_fixture()
      assert Contacts.list_contact_addresses() == [contact_address]
    end

    test "get_contact_address!/1 returns the contact_address with given id" do
      contact_address = contact_address_fixture()
      assert Contacts.get_contact_address!(contact_address.id) == contact_address
    end

    test "create_contact_address/1 with valid data creates a contact_address" do
      assert {:ok, %ContactAddress{} = contact_address} = Contacts.create_contact_address(@valid_attrs)
      assert contact_address.address == "some address"
      assert contact_address.email == "some email"
      assert contact_address.phone == "some phone"
    end

    test "create_contact_address/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Contacts.create_contact_address(@invalid_attrs)
    end

    test "update_contact_address/2 with valid data updates the contact_address" do
      contact_address = contact_address_fixture()
      assert {:ok, %ContactAddress{} = contact_address} = Contacts.update_contact_address(contact_address, @update_attrs)
      assert contact_address.address == "some updated address"
      assert contact_address.email == "some updated email"
      assert contact_address.phone == "some updated phone"
    end

    test "update_contact_address/2 with invalid data returns error changeset" do
      contact_address = contact_address_fixture()
      assert {:error, %Ecto.Changeset{}} = Contacts.update_contact_address(contact_address, @invalid_attrs)
      assert contact_address == Contacts.get_contact_address!(contact_address.id)
    end

    test "delete_contact_address/1 deletes the contact_address" do
      contact_address = contact_address_fixture()
      assert {:ok, %ContactAddress{}} = Contacts.delete_contact_address(contact_address)
      assert_raise Ecto.NoResultsError, fn -> Contacts.get_contact_address!(contact_address.id) end
    end

    test "change_contact_address/1 returns a contact_address changeset" do
      contact_address = contact_address_fixture()
      assert %Ecto.Changeset{} = Contacts.change_contact_address(contact_address)
    end
  end
end
