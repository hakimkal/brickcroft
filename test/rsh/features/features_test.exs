defmodule Brickcroft.FeaturesTest do
  use Brickcroft.DataCase

  alias Brickcroft.Features

  describe "features" do
    alias Brickcroft.Features.Feature

    @valid_attrs %{details: "some details", title: "some title"}
    @update_attrs %{details: "some updated details", title: "some updated title"}
    @invalid_attrs %{details: nil, title: nil}

    def feature_fixture(attrs \\ %{}) do
      {:ok, feature} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Features.create_feature()

      feature
    end

    test "list_features/0 returns all features" do
      feature = feature_fixture()
      assert Features.list_features() == [feature]
    end

    test "get_feature!/1 returns the feature with given id" do
      feature = feature_fixture()
      assert Features.get_feature!(feature.id) == feature
    end

    test "create_feature/1 with valid data creates a feature" do
      assert {:ok, %Feature{} = feature} = Features.create_feature(@valid_attrs)
      assert feature.details == "some details"
      assert feature.title == "some title"
    end

    test "create_feature/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Features.create_feature(@invalid_attrs)
    end

    test "update_feature/2 with valid data updates the feature" do
      feature = feature_fixture()
      assert {:ok, %Feature{} = feature} = Features.update_feature(feature, @update_attrs)
      assert feature.details == "some updated details"
      assert feature.title == "some updated title"
    end

    test "update_feature/2 with invalid data returns error changeset" do
      feature = feature_fixture()
      assert {:error, %Ecto.Changeset{}} = Features.update_feature(feature, @invalid_attrs)
      assert feature == Features.get_feature!(feature.id)
    end

    test "delete_feature/1 deletes the feature" do
      feature = feature_fixture()
      assert {:ok, %Feature{}} = Features.delete_feature(feature)
      assert_raise Ecto.NoResultsError, fn -> Features.get_feature!(feature.id) end
    end

    test "change_feature/1 returns a feature changeset" do
      feature = feature_fixture()
      assert %Ecto.Changeset{} = Features.change_feature(feature)
    end
  end

  describe "e" do
    alias Brickcroft.Features.FeatureImage

    @valid_attrs %{feature_images: "some feature_images", image_path: "some image_path"}
    @update_attrs %{feature_images: "some updated feature_images", image_path: "some updated image_path"}
    @invalid_attrs %{feature_images: nil, image_path: nil}

    def feature_image_fixture(attrs \\ %{}) do
      {:ok, feature_image} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Features.create_feature_image()

      feature_image
    end

    test "list_e/0 returns all e" do
      feature_image = feature_image_fixture()
      assert Features.list_e() == [feature_image]
    end

    test "get_feature_image!/1 returns the feature_image with given id" do
      feature_image = feature_image_fixture()
      assert Features.get_feature_image!(feature_image.id) == feature_image
    end

    test "create_feature_image/1 with valid data creates a feature_image" do
      assert {:ok, %FeatureImage{} = feature_image} = Features.create_feature_image(@valid_attrs)
      assert feature_image.feature_images == "some feature_images"
      assert feature_image.image_path == "some image_path"
    end

    test "create_feature_image/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Features.create_feature_image(@invalid_attrs)
    end

    test "update_feature_image/2 with valid data updates the feature_image" do
      feature_image = feature_image_fixture()
      assert {:ok, %FeatureImage{} = feature_image} = Features.update_feature_image(feature_image, @update_attrs)
      assert feature_image.feature_images == "some updated feature_images"
      assert feature_image.image_path == "some updated image_path"
    end

    test "update_feature_image/2 with invalid data returns error changeset" do
      feature_image = feature_image_fixture()
      assert {:error, %Ecto.Changeset{}} = Features.update_feature_image(feature_image, @invalid_attrs)
      assert feature_image == Features.get_feature_image!(feature_image.id)
    end

    test "delete_feature_image/1 deletes the feature_image" do
      feature_image = feature_image_fixture()
      assert {:ok, %FeatureImage{}} = Features.delete_feature_image(feature_image)
      assert_raise Ecto.NoResultsError, fn -> Features.get_feature_image!(feature_image.id) end
    end

    test "change_feature_image/1 returns a feature_image changeset" do
      feature_image = feature_image_fixture()
      assert %Ecto.Changeset{} = Features.change_feature_image(feature_image)
    end
  end
end
