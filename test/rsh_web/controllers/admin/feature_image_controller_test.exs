defmodule BrickcroftWeb.Admin.FeatureImageControllerTest do
  use BrickcroftWeb.ConnCase

  alias Brickcroft.Features

  @create_attrs %{feature_images: "some feature_images", image_path: "some image_path"}
  @update_attrs %{feature_images: "some updated feature_images", image_path: "some updated image_path"}
  @invalid_attrs %{feature_images: nil, image_path: nil}

  def fixture(:feature_image) do
    {:ok, feature_image} = Features.create_feature_image(@create_attrs)
    feature_image
  end

  describe "index" do
    test "lists all e", %{conn: conn} do
      conn = get(conn, Routes.admin_feature_image_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing E"
    end
  end

  describe "new feature_image" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.admin_feature_image_path(conn, :new))
      assert html_response(conn, 200) =~ "New Feature image"
    end
  end

  describe "create feature_image" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.admin_feature_image_path(conn, :create), feature_image: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.admin_feature_image_path(conn, :show, id)

      conn = get(conn, Routes.admin_feature_image_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Feature image"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.admin_feature_image_path(conn, :create), feature_image: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Feature image"
    end
  end

  describe "edit feature_image" do
    setup [:create_feature_image]

    test "renders form for editing chosen feature_image", %{conn: conn, feature_image: feature_image} do
      conn = get(conn, Routes.admin_feature_image_path(conn, :edit, feature_image))
      assert html_response(conn, 200) =~ "Edit Feature image"
    end
  end

  describe "update feature_image" do
    setup [:create_feature_image]

    test "redirects when data is valid", %{conn: conn, feature_image: feature_image} do
      conn = put(conn, Routes.admin_feature_image_path(conn, :update, feature_image), feature_image: @update_attrs)
      assert redirected_to(conn) == Routes.admin_feature_image_path(conn, :show, feature_image)

      conn = get(conn, Routes.admin_feature_image_path(conn, :show, feature_image))
      assert html_response(conn, 200) =~ "some updated feature_images"
    end

    test "renders errors when data is invalid", %{conn: conn, feature_image: feature_image} do
      conn = put(conn, Routes.admin_feature_image_path(conn, :update, feature_image), feature_image: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Feature image"
    end
  end

  describe "delete feature_image" do
    setup [:create_feature_image]

    test "deletes chosen feature_image", %{conn: conn, feature_image: feature_image} do
      conn = delete(conn, Routes.admin_feature_image_path(conn, :delete, feature_image))
      assert redirected_to(conn) == Routes.admin_feature_image_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.admin_feature_image_path(conn, :show, feature_image))
      end
    end
  end

  defp create_feature_image(_) do
    feature_image = fixture(:feature_image)
    {:ok, feature_image: feature_image}
  end
end
