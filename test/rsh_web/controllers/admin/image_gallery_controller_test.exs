defmodule BrickcroftWeb.ImageGalleryControllerTest do
  use BrickcroftWeb.ConnCase

  alias Brickcroft.Gallery

  @create_attrs %{image_path: "some image_path", section: "some section", subtitle: "some subtitle", title: "some title"}
  @update_attrs %{image_path: "some updated image_path", section: "some updated section", subtitle: "some updated subtitle", title: "some updated title"}
  @invalid_attrs %{image_path: nil, section: nil, subtitle: nil, title: nil}

  def fixture(:image_gallery) do
    {:ok, image_gallery} = Gallery.create_image_gallery(@create_attrs)
    image_gallery
  end

  describe "index" do
    test "lists all imagegalleries", %{conn: conn} do
      conn = get(conn, Routes.image_gallery_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Imagegalleries"
    end
  end

  describe "new image_gallery" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.image_gallery_path(conn, :new))
      assert html_response(conn, 200) =~ "New Image gallery"
    end
  end

  describe "create image_gallery" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.image_gallery_path(conn, :create), image_gallery: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.image_gallery_path(conn, :show, id)

      conn = get(conn, Routes.image_gallery_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Image gallery"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.image_gallery_path(conn, :create), image_gallery: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Image gallery"
    end
  end

  describe "edit image_gallery" do
    setup [:create_image_gallery]

    test "renders form for editing chosen image_gallery", %{conn: conn, image_gallery: image_gallery} do
      conn = get(conn, Routes.image_gallery_path(conn, :edit, image_gallery))
      assert html_response(conn, 200) =~ "Edit Image gallery"
    end
  end

  describe "update image_gallery" do
    setup [:create_image_gallery]

    test "redirects when data is valid", %{conn: conn, image_gallery: image_gallery} do
      conn = put(conn, Routes.image_gallery_path(conn, :update, image_gallery), image_gallery: @update_attrs)
      assert redirected_to(conn) == Routes.image_gallery_path(conn, :show, image_gallery)

      conn = get(conn, Routes.image_gallery_path(conn, :show, image_gallery))
      assert html_response(conn, 200) =~ "some updated image_path"
    end

    test "renders errors when data is invalid", %{conn: conn, image_gallery: image_gallery} do
      conn = put(conn, Routes.image_gallery_path(conn, :update, image_gallery), image_gallery: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Image gallery"
    end
  end

  describe "delete image_gallery" do
    setup [:create_image_gallery]

    test "deletes chosen image_gallery", %{conn: conn, image_gallery: image_gallery} do
      conn = delete(conn, Routes.image_gallery_path(conn, :delete, image_gallery))
      assert redirected_to(conn) == Routes.image_gallery_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.image_gallery_path(conn, :show, image_gallery))
      end
    end
  end

  defp create_image_gallery(_) do
    image_gallery = fixture(:image_gallery)
    {:ok, image_gallery: image_gallery}
  end
end
