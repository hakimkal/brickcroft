defmodule BrickcroftWeb.Admin.ContactAddressControllerTest do
  use BrickcroftWeb.ConnCase

  alias Brickcroft.Contacts

  @create_attrs %{address: "some address", email: "some email", phone: "some phone"}
  @update_attrs %{address: "some updated address", email: "some updated email", phone: "some updated phone"}
  @invalid_attrs %{address: nil, email: nil, phone: nil}

  def fixture(:contact_address) do
    {:ok, contact_address} = Contacts.create_contact_address(@create_attrs)
    contact_address
  end

  describe "index" do
    test "lists all contact_addresses", %{conn: conn} do
      conn = get(conn, Routes.admin_contact_address_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Contact addresses"
    end
  end

  describe "new contact_address" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.admin_contact_address_path(conn, :new))
      assert html_response(conn, 200) =~ "New Contact address"
    end
  end

  describe "create contact_address" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.admin_contact_address_path(conn, :create), contact_address: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.admin_contact_address_path(conn, :show, id)

      conn = get(conn, Routes.admin_contact_address_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Contact address"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.admin_contact_address_path(conn, :create), contact_address: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Contact address"
    end
  end

  describe "edit contact_address" do
    setup [:create_contact_address]

    test "renders form for editing chosen contact_address", %{conn: conn, contact_address: contact_address} do
      conn = get(conn, Routes.admin_contact_address_path(conn, :edit, contact_address))
      assert html_response(conn, 200) =~ "Edit Contact address"
    end
  end

  describe "update contact_address" do
    setup [:create_contact_address]

    test "redirects when data is valid", %{conn: conn, contact_address: contact_address} do
      conn = put(conn, Routes.admin_contact_address_path(conn, :update, contact_address), contact_address: @update_attrs)
      assert redirected_to(conn) == Routes.admin_contact_address_path(conn, :show, contact_address)

      conn = get(conn, Routes.admin_contact_address_path(conn, :show, contact_address))
      assert html_response(conn, 200) =~ "some updated address"
    end

    test "renders errors when data is invalid", %{conn: conn, contact_address: contact_address} do
      conn = put(conn, Routes.admin_contact_address_path(conn, :update, contact_address), contact_address: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Contact address"
    end
  end

  describe "delete contact_address" do
    setup [:create_contact_address]

    test "deletes chosen contact_address", %{conn: conn, contact_address: contact_address} do
      conn = delete(conn, Routes.admin_contact_address_path(conn, :delete, contact_address))
      assert redirected_to(conn) == Routes.admin_contact_address_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.admin_contact_address_path(conn, :show, contact_address))
      end
    end
  end

  defp create_contact_address(_) do
    contact_address = fixture(:contact_address)
    {:ok, contact_address: contact_address}
  end
end
