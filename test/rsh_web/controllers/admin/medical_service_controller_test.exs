defmodule BrickcroftWeb.Admin.MedicalServiceControllerTest do
  use BrickcroftWeb.ConnCase

  alias Brickcroft.MedicalServices

  @create_attrs %{details: "some details", title: "some title"}
  @update_attrs %{details: "some updated details", title: "some updated title"}
  @invalid_attrs %{details: nil, title: nil}

  def fixture(:medical_service) do
    {:ok, medical_service} = MedicalServices.create_medical_service(@create_attrs)
    medical_service
  end

  describe "index" do
    test "lists all medicalservices", %{conn: conn} do
      conn = get(conn, Routes.admin_medical_service_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Medicalservices"
    end
  end

  describe "new medical_service" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.admin_medical_service_path(conn, :new))
      assert html_response(conn, 200) =~ "New Medical service"
    end
  end

  describe "create medical_service" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.admin_medical_service_path(conn, :create), medical_service: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.admin_medical_service_path(conn, :show, id)

      conn = get(conn, Routes.admin_medical_service_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Medical service"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.admin_medical_service_path(conn, :create), medical_service: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Medical service"
    end
  end

  describe "edit medical_service" do
    setup [:create_medical_service]

    test "renders form for editing chosen medical_service", %{conn: conn, medical_service: medical_service} do
      conn = get(conn, Routes.admin_medical_service_path(conn, :edit, medical_service))
      assert html_response(conn, 200) =~ "Edit Medical service"
    end
  end

  describe "update medical_service" do
    setup [:create_medical_service]

    test "redirects when data is valid", %{conn: conn, medical_service: medical_service} do
      conn = put(conn, Routes.admin_medical_service_path(conn, :update, medical_service), medical_service: @update_attrs)
      assert redirected_to(conn) == Routes.admin_medical_service_path(conn, :show, medical_service)

      conn = get(conn, Routes.admin_medical_service_path(conn, :show, medical_service))
      assert html_response(conn, 200) =~ "some updated details"
    end

    test "renders errors when data is invalid", %{conn: conn, medical_service: medical_service} do
      conn = put(conn, Routes.admin_medical_service_path(conn, :update, medical_service), medical_service: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Medical service"
    end
  end

  describe "delete medical_service" do
    setup [:create_medical_service]

    test "deletes chosen medical_service", %{conn: conn, medical_service: medical_service} do
      conn = delete(conn, Routes.admin_medical_service_path(conn, :delete, medical_service))
      assert redirected_to(conn) == Routes.admin_medical_service_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.admin_medical_service_path(conn, :show, medical_service))
      end
    end
  end

  defp create_medical_service(_) do
    medical_service = fixture(:medical_service)
    {:ok, medical_service: medical_service}
  end
end
