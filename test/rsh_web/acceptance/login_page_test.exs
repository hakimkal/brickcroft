defmodule BrickcroftWeb.Acceptance.LoginPageTest do
  @moduledoc false

  use Brickcroft.DataCase
  use Hound.Helpers

  hound_session()

  test "login form has email and password form" do
    navigate_to "/admin"
    assert current_url == "/admin"
  end

  test "presence forgot_password link for password recovery" do

  end
end
