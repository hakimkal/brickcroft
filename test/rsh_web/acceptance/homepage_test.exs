defmodule BrickcroftWeb.Acceptance.HomepageTest do


  use Brickcroft.DataCase
  use Hound.Helpers

  hound_session()


  setup do

    #alias Mango.Repo
    #alias Mango.Catalog.Product


    ##Given ##
    # there are two products Apple and Tomato priced at 100 and 50
    #categorized under `fruits` and `vegetable` respectively
    #Repo.insert %Product{sku: "A123", name: "Tomato", price: 50, category: "vegetables", is_seasonal: false}
    #Repo.insert %Product{sku: "B232", name: "Apple", price: 100, category: "fruits", is_seasonal: true}
    :ok



  end
  test "presence of royal specialist hospital in header" do

    ## GIVEN ##


    ##WHEN ##
    # I navigate to the homepage
    navigate_to("/")

    #Then I expect the page title to be uppercase of  "Royal Specialist Hospital"
    page_title = find_element(:css, ".jumbotron")
                 |> find_within_element(:tag, "h2") |>  visible_text()
    assert page_title == String.upcase "Royal Specialist Hospital"

    #And I expect Apple to be the seasonal product displayed



  end

end
