defmodule Brickcroft.ProjectsTest do
  use Brickcroft.DataCase

  alias Brickcroft.Projects

  describe "categories" do
    alias Brickcroft.Projects.Category

    @valid_attrs %{description: "some description", name: "some name"}
    @update_attrs %{description: "some updated description", name: "some updated name"}
    @invalid_attrs %{description: nil, name: nil}

    def category_fixture(attrs \\ %{}) do
      {:ok, category} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Projects.create_category()

      category
    end

    test "list_categories/0 returns all categories" do
      category = category_fixture()
      assert Projects.list_categories() == [category]
    end

    test "get_category!/1 returns the category with given id" do
      category = category_fixture()
      assert Projects.get_category!(category.id) == category
    end

    test "create_category/1 with valid data creates a category" do
      assert {:ok, %Category{} = category} = Projects.create_category(@valid_attrs)
      assert category.description == "some description"
      assert category.name == "some name"
    end

    test "create_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Projects.create_category(@invalid_attrs)
    end

    test "update_category/2 with valid data updates the category" do
      category = category_fixture()
      assert {:ok, %Category{} = category} = Projects.update_category(category, @update_attrs)
      assert category.description == "some updated description"
      assert category.name == "some updated name"
    end

    test "update_category/2 with invalid data returns error changeset" do
      category = category_fixture()
      assert {:error, %Ecto.Changeset{}} = Projects.update_category(category, @invalid_attrs)
      assert category == Projects.get_category!(category.id)
    end

    test "delete_category/1 deletes the category" do
      category = category_fixture()
      assert {:ok, %Category{}} = Projects.delete_category(category)
      assert_raise Ecto.NoResultsError, fn -> Projects.get_category!(category.id) end
    end

    test "change_category/1 returns a category changeset" do
      category = category_fixture()
      assert %Ecto.Changeset{} = Projects.change_category(category)
    end
  end

  describe "sub_categories" do
    alias Brickcroft.Projects.SubCategory

    @valid_attrs %{description: "some description", name: "some name"}
    @update_attrs %{description: "some updated description", name: "some updated name"}
    @invalid_attrs %{description: nil, name: nil}

    def sub_category_fixture(attrs \\ %{}) do
      {:ok, sub_category} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Projects.create_sub_category()

      sub_category
    end

    test "list_sub_categories/0 returns all sub_categories" do
      sub_category = sub_category_fixture()
      assert Projects.list_sub_categories() == [sub_category]
    end

    test "get_sub_category!/1 returns the sub_category with given id" do
      sub_category = sub_category_fixture()
      assert Projects.get_sub_category!(sub_category.id) == sub_category
    end

    test "create_sub_category/1 with valid data creates a sub_category" do
      assert {:ok, %SubCategory{} = sub_category} = Projects.create_sub_category(@valid_attrs)
      assert sub_category.description == "some description"
      assert sub_category.name == "some name"
    end

    test "create_sub_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Projects.create_sub_category(@invalid_attrs)
    end

    test "update_sub_category/2 with valid data updates the sub_category" do
      sub_category = sub_category_fixture()
      assert {:ok, %SubCategory{} = sub_category} = Projects.update_sub_category(sub_category, @update_attrs)
      assert sub_category.description == "some updated description"
      assert sub_category.name == "some updated name"
    end

    test "update_sub_category/2 with invalid data returns error changeset" do
      sub_category = sub_category_fixture()
      assert {:error, %Ecto.Changeset{}} = Projects.update_sub_category(sub_category, @invalid_attrs)
      assert sub_category == Projects.get_sub_category!(sub_category.id)
    end

    test "delete_sub_category/1 deletes the sub_category" do
      sub_category = sub_category_fixture()
      assert {:ok, %SubCategory{}} = Projects.delete_sub_category(sub_category)
      assert_raise Ecto.NoResultsError, fn -> Projects.get_sub_category!(sub_category.id) end
    end

    test "change_sub_category/1 returns a sub_category changeset" do
      sub_category = sub_category_fixture()
      assert %Ecto.Changeset{} = Projects.change_sub_category(sub_category)
    end
  end
end
