# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Brickcroft.Repo.insert!(%Brickcroft.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Brickcroft.Repo
alias Brickcroft.Administration.User
import Comeonin.Bcrypt , only: [hashpwsalt: 1]
pswd = hashpwsalt("dansaw")
user  =  %User{ email: "hakimkal@gmail.com", firstname: "Abdulhakim", lastname: "Haliru" ,password_hash: pswd , phone: "+2348032898122"}
Repo.insert user
