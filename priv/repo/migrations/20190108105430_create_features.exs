defmodule Brickcroft.Repo.Migrations.CreateFeatures do
  use Ecto.Migration

  def change do
    create table(:features) do
      add :title, :citext
      add :details, :text

      timestamps()

    end
    create unique_index(:features, [:title])

  end
end
