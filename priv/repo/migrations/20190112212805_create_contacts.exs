defmodule Brickcroft.Repo.Migrations.CreateContacts do
  use Ecto.Migration

  def change do
    create table(:contacts) do
      add :location, :citext

      timestamps()
    end

    create unique_index(:contacts, [:location])
  end
end
