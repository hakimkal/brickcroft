defmodule Brickcroft.Repo.Migrations.CreateGeneralsettings do
  use Ecto.Migration

  def change do
    create table(:generalsettings) do
      add :mission, :text
      add :vision, :text
      add :mdnote, :text
      add :sales_caption, :string
      add :sales_note, :text

      timestamps()
    end

  end
end
