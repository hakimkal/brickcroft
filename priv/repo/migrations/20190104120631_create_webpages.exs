defmodule Brickcroft.Repo.Migrations.CreateTitle do
  use Ecto.Migration

  def change do
    create table(:webpages) do
      add :title, :citext
      add :html_template, :string
      add :content, :string
      add :slug, :string
      add :subtitle, :string

      timestamps()
    end

  end
end
