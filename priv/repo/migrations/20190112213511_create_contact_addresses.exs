defmodule Brickcroft.Repo.Migrations.CreateContactAddresses do
  use Ecto.Migration

  def change do
    create table(:contact_addresses) do
      add :address, :citext
      add :email, :citext
      add :phone, :citext

      timestamps()
    end
    create unique_index(:contact_addresses, [:address, :email])

  end
end
