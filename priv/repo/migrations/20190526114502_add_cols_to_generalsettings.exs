defmodule Brickcroft.Repo.Migrations.AddColsToGeneralsettings do
  use Ecto.Migration

  def change do
    alter table(:generalsettings) do
      add :footer_brief_pitch ,:text
      add :footer_brief_service, :text
      add :header_address, :string
      add :header_phones, :string
      add :header_email, :string
      remove :sales_caption
      remove :sales_note
    end
  end
end
