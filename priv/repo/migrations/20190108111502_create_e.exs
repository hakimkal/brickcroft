defmodule Brickcroft.Repo.Migrations.CreateE do
  use Ecto.Migration

  def change do
    create table(:feature_images) do

      add :image_path, :string
      add :feature_id, references(:features)

      timestamps()
    end

  end
end
