defmodule Brickcroft.Repo.Migrations.CreateMedicalservicepacks do
  use Ecto.Migration

  def change do
    create table(:medicalservicepacks) do
      add :package_detail, :text
      add :medical_service_id, references(:medicalservices, on_delete: :nothing)

      timestamps()
    end

    create index(:medicalservicepacks, [:medical_service_id])
  end
end
