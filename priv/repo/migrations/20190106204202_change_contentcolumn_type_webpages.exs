defmodule Brickcroft.Repo.Migrations.ChangeContentcolumnTypeWebpages do
  use Ecto.Migration

  def change do
    alter table(:webpages) do
      modify(:content, :text)
    end
  end
end
