defmodule Brickcroft.Repo.Migrations.AddActiveColToUsers do
  use Ecto.Migration

  def change do
 alter table(:users) do
   add :active ,:boolean
 end
  end
end
