defmodule Brickcroft.Repo.Migrations.AddValuesFieldToGeneralsettings do
  use Ecto.Migration

  def change do
alter table (:generalsettings) do
  add :values, :text
end
  end
end
