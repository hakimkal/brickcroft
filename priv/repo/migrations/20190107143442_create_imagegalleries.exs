defmodule Brickcroft.Repo.Migrations.CreateImagegalleries do
  use Ecto.Migration

  def change do
    create table(:imagegalleries) do
      add :image_path, :string
      add :title, :string
      add :subtitle, :string
      add :section, :string

      timestamps()
    end

  end
end
