defmodule Brickcroft.Repo.Migrations.CreateMedicalservices do
  use Ecto.Migration

  def change do
    create table(:medicalservices) do
      add :title, :string
      add :details, :text

      timestamps()
    end

  end
end
