defmodule Brickcroft.Repo.Migrations.CreateTeams do
  use Ecto.Migration

  def change do
    create table(:teams) do
      add :name, :string
      add :designation, :string
      add :profile, :text

      timestamps()
    end

  end
end
