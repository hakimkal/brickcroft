defmodule Brickcroft.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    execute "CREATE EXTENSION IF NOT EXISTS citext"
    create table(:users) do
      add :email, :citext
      add :firstname, :string
      add :lastname, :string
      add :password_hash, :string
      add :phone, :string

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
