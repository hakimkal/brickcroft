defmodule Brickcroft.Repo.Migrations.CreateContactIdContactAddress do
  use Ecto.Migration

  def change do
    alter table(:contact_addresses) do
      add :contact_id, references(:contacts)
    end
  end
end
