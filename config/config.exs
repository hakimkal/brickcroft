# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :brickcroft,
  ecto_repos: [Brickcroft.Repo]

# Configures the endpoint
config :brickcroft, BrickcroftWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Jus6kUcFaTgu/XYaTF7SpX73wfJCPiAi57gz7BR9WhqW0QVwm+RaYJrOZlIaElnq",
  render_errors: [view: BrickcroftWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Brickcroft.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason




config :recaptcha,
       public_key: "6Ld1XZcUAAAAAMIpT2xHPeecYOc5uAdtwasK3R-R",
       secret: "6Ld1XZcUAAAAAJ2FCnStmg-PL9Ci9LlrUTpGgmV7"

config :brickcroft, Brickcroft.Mailer,
       adapter: Bamboo.MailgunAdapter,
       api_key: "key-0a28f9ada474b70b853763a124e50efb",
       domain: "agnopayroll.com",

       #username: System.get_env("SMTP_USERNAME"),
       #password: System.get_env("SMTP_PASSWORD"),
       ssl: false, # can be `true`
       retries: 1



# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
