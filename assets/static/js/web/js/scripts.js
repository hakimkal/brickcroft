$(document).ready(function(){

  (function() {

    var filterDivs = $('#gallery .filter');                  // Store all images
    var $buttons = $('#buttons');                   // Store buttons element
    var tagged = {};                                // Create tagged object
  
    filterDivs.each(function() {                         // Loop through images and
      var img = this;                               // Store img in variable
      var tags = $(this).data('tags');              // Get this element's tags
  
      if (tags) {                                   // If the element had tags
        tags.split(',').forEach(function(tagName) { // Split at comma and
  
          if (tagged[tagName] == null) {            // If object doesn't have tag
            tagged[tagName] = [];                   // Add empty array to object
          }
          tagged[tagName].push(img);                // Add the image to the array
        });
      }
    });
  
    $('<button/>', {                                 // Create empty button
      text: 'Show All',                              // Add text 'show all'
      class: 'active',                               // Make it active
      click: function() {                            // Add onclick handler to
        $(this)                                      // Get the clicked on button
          .addClass('active')                        // Add the class of active
          .siblings()                                // Get its siblings
          .removeClass('active');                    // Remove active from siblings
        filterDivs.hide().slideDown(500);                 // Show all images
      }
    }).appendTo($buttons);                           // Add to buttons
  
    $.each(tagged, function(tagName) {               // For each tag name
      $('<button/>', {                               // Create empty button
        text: tagName + ' (' + tagged[tagName].length + ')', // Add tag name
        click: function() {                          // Add click handler
          $(this)                                    // The button clicked on
            .addClass('active')                      // Make clicked item active
            .siblings()                              // Get its siblings
            .removeClass('active');                  // Remove active from siblings
          filterDivs                                      // With all of the images
            .hide()                                  // Hide them
            .filter(tagged[tagName])                 // Find ones with this tag
            .fadeIn(500);                                // Show just those images
        }
      }).appendTo($buttons);                         // Add to the buttons
    });
  
  }());

  $('#carsoule1').slick({
	    dots: false,
      arrows: true,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      nextArrow: $('#carsoule1Next'),
      prevArrow: $('#carsoule1Prev')
  });
  
  $('#carsoule2').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    nextArrow: $('#carsoule2Next'),
    prevArrow: $('#carsoule2Prev')
});
    
  });